// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path');

// eslint-disable-next-line @typescript-eslint/no-var-requires
const nodeExternals = require('webpack-node-externals');

module.exports = {
  entry: './src/index.ts',
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env', '@babel/preset-react'],
            },
          },
          'ts-loader',
        ],
      },
      {
        test: /\.svg$/i,
        issuer: /\.[jt]sx?$/,
        use: ['@svgr/webpack', 'url-loader'],
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.json'],
    modules: ['node_modules'],
    alias: {
      assets: path.resolve(__dirname, 'src', 'assets'),
    },
  },
  target: 'node',
  externals: [nodeExternals()],
  output: {
    globalObject: 'this',
    filename: 'index.js',
    path: path.resolve(__dirname, 'dist'),
    library: {
      name: '@ECMAScriptOS/UI-custom-components',
      type: 'umd',
      umdNamedDefine: true,
    },
  },
};
