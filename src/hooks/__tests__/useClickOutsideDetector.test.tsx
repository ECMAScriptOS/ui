import { fireEvent, renderHook } from '@testing-library/react';

import { useClickOutsideDetector } from '../useClickOutsideDetector';

describe('__custom hook: useClickOutsideDetector', () => {
  const mockHandler = jest.fn();
  const mockRef = { current: document.createElement('div') };

  it('should call handler after clicking outside element', () => {
    renderHook(() => useClickOutsideDetector(mockRef, mockHandler));

    fireEvent.mouseUp(document.body);

    expect(mockHandler).toBeCalledWith(false);
  });
});
