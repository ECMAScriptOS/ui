import { RefObject, useCallback, useEffect } from 'react';

export const useClickOutsideDetector = (
  elementRef: RefObject<HTMLDivElement>,
  setIsVisible: (isVisible: boolean) => void
) => {
  const checkClickedElement = useCallback(
    (event: MouseEvent) => {
      if (elementRef.current?.contains(event.target as HTMLElement)) return;

      setIsVisible(false);
    },
    [elementRef, setIsVisible]
  );

  useEffect(() => {
    // ToDo: Should use another event
    document.addEventListener('mouseup', checkClickedElement);

    return () => document.removeEventListener('mouseup', checkClickedElement);
  }, [elementRef, checkClickedElement]);
};
