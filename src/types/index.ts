import { FormikConfig } from 'formik';

export type LabelPositionType = 'top' | 'bottom' | 'left' | 'right';

export type ColorType =
  | 'error'
  | 'success'
  | 'warning'
  | 'primary'
  | 'secondary'
  | 'white';

export type formikSubmitEvent = FormikConfig<any>['onSubmit'];
