import type { Meta, StoryObj } from '@storybook/react';
import { withRouter } from 'storybook-addon-remix-react-router';

import { ALink } from '../../../components/ALink/ALink';

const args = {
  children: 'Link content',
  to: 'test',
};

const meta = {
  title: 'Components/ALink',
  component: ALink,
  tags: ['autodocs'],
  decorators: [withRouter],
} satisfies Meta<typeof ALink>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    ...args,
  },
};
