import withFormik from '@bbbtech/storybook-formik';
import { Meta, StoryObj } from '@storybook/react';

import { ACheckbox } from '../../../components/ACheckbox/ACheckbox';
import { IACheckbox } from '../../../components/ACheckbox/type';

const args: IACheckbox = {
  id: 'checkboxID',
  labelPosition: 'right',
  name: 'checkboxDefault',
  label: 'Label content',
};

const meta = {
  title: 'Components/ACheckbox',
  component: ACheckbox,
  tags: ['autodocs'],
  decorators: [withFormik],
} satisfies Meta<typeof ACheckbox>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args,
};

export const Checked: Story = {
  args: {
    ...args,
    name: 'checkboxChecked',
  },
  parameters: {
    formik: {
      initialValues: { checkboxChecked: true },
    },
  },
};

export const Disabled: Story = {
  args: {
    ...args,
    name: 'checkboxDisabled',
    disabled: true,
  },
};
