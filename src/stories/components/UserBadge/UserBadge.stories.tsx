import type { Meta, StoryObj } from '@storybook/react';
import { withRouter } from 'storybook-addon-remix-react-router';

import { IUserBadge, UserBadge } from '../../../components';

const args: IUserBadge = {
  firstName: 'Name',
  lastName: 'Surname',
  userPageLink: 'test',
  age: '41 years',
  nationality: 'BY',
  isOnline: true,
  photoURL:
    'https://static.vecteezy.com/system/resources/thumbnails/011/991/883/small/the-concept-was-discouraged-and-hopeless-a-lonely-and-heartbreaking-concept-free-photo.jpg',
};

const meta = {
  title: 'Components/UserBadge',
  component: UserBadge,
  tags: ['autodocs'],
  decorators: [withRouter],
} satisfies Meta<typeof UserBadge>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Normal: Story = {
  args: {
    ...args,
  },
};

export const Minimized: Story = {
  args: {
    ...args,
    type: 'minimized',
  },
};

export const ExtraMinimized: Story = {
  args: {
    ...args,
    type: 'extraMinimized',
  },
};
