import type { Meta, StoryObj } from '@storybook/react';

import { Message } from '../../../components/Message';

const args = {
  content: 'Some message',
  isAuthor: true,
  isRead: true,
  expandButtonContent: 'More',
  shrinkButtonContent: 'Less',
};

const meta = {
  title: 'Components/Message',
  component: Message,
  tags: ['autodocs'],
} satisfies Meta<typeof Message>;

export default meta;
type Story = StoryObj<typeof meta>;

export const SentMessage: Story = {
  args,
};

export const ReceivedMessage: Story = {
  args: {
    ...args,
    isAuthor: false,
  },
};

export const MessageIsNotRead: Story = {
  args: {
    ...args,
    isRead: false,
  },
};

export const MessageWithLongContent = {
  args: {
    ...args,
    content:
      'A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. A very long content. ',
  },
};
