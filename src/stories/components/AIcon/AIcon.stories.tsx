import { Meta, StoryObj } from '@storybook/react';
import { ReactComponent as Icon } from 'assets/icons/common/checked.svg';
import { ReactComponent as SuccessIcon } from 'assets/icons/common/success.svg';
import { ReactComponent as ErrorIcon } from 'assets/icons/common/error.svg';
import { ReactComponent as WarningIcon } from 'assets/icons/common/warning.svg';

import { AIcon } from '../../../components';

const meta = {
  title: 'Components/AIcon',
  component: AIcon,
  tags: ['autodocs'],
} satisfies Meta<typeof AIcon>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
  args: { children: <Icon title="checked" /> },
};

export const Success: Story = {
  args: { children: <SuccessIcon title="success" />, color: 'success' },
};

export const Error: Story = {
  args: { children: <ErrorIcon title="error" />, color: 'error' },
};

export const Warning: Story = {
  args: { children: <WarningIcon title="warning" />, color: 'warning' },
};
