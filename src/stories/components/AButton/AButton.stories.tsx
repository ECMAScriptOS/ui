import type { Meta, StoryObj } from '@storybook/react';
import { ReactComponent as Icon } from 'assets/icons/common/checked.svg';

import { AButton } from '../../../components';

const args = {
  children: 'Button content',
  handleClick: () => null,
};

const meta = {
  title: 'Components/AButton',
  component: AButton,
  tags: ['autodocs'],
} satisfies Meta<typeof AButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
  args: {
    ...args,
  },
};

export const Secondary: Story = {
  args: {
    ...args,
    color: 'secondary',
  },
};

export const Disabled: Story = {
  args: {
    ...args,
    disabled: true,
  },
};

export const Filled: Story = {
  args: {
    ...args,
    filled: true,
  },
};

export const PrimaryIconType: Story = {
  args: {
    ...args,
    filled: true,
    icon: <Icon title="calendar" />,
  },
};

export const SecondaryIconType: Story = {
  args: {
    ...args,
    color: 'secondary',
    filled: true,
    icon: <Icon title="calendar" />,
  },
};

export const DisabledIconType: Story = {
  args: {
    ...args,
    disabled: true,
    filled: true,
    icon: <Icon title="calendar" />,
  },
};

export const TransparentIconType: Story = {
  args: {
    ...args,
    icon: <Icon title="calendar" />,
  },
};

export const RoundedIconType: Story = {
  args: {
    ...args,
    filled: true,
    rounded: true,
    icon: <Icon title="calendar" />,
  },
};

export const RoundedErrorIconType: Story = {
  args: {
    ...args,
    rounded: true,
    color: 'error',
    icon: <Icon title="calendar" />,
  },
};
