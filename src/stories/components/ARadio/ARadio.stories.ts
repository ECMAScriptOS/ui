import withFormik from '@bbbtech/storybook-formik';
import type { Meta, StoryObj } from '@storybook/react';

import { ARadio } from '../../../components/ARadio/ARadio';
import { IARadio } from '../../../components/ARadio/type';

const args: IARadio = {
  id: 'radioID',
  name: 'radioDefault',
  value: 'value_1',
  label: 'Label',
};

const meta = {
  title: 'Components/ARadio',
  component: ARadio,
  tags: ['autodocs'],
  decorators: [withFormik],
} satisfies Meta<typeof ARadio>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args,
};

export const Checked: Story = {
  args: {
    ...args,
    name: 'radioChecked',
  },
  parameters: {
    formik: {
      initialValues: { radioChecked: args.value },
    },
  },
};

export const Disabled: Story = {
  args: {
    ...args,
    name: 'radioDisabled',
    disabled: true,
  },
};
