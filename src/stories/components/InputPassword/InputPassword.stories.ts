import withFormik from '@bbbtech/storybook-formik';
import { Meta, StoryObj } from '@storybook/react';

import { IInputPassword } from '../../../components/InputPassword/type';
import { InputPassword } from '../../../components/InputPassword/InputPassword';

const args: IInputPassword = {
  id: 'inputPasswordID',
  name: 'name_1',
  label: 'Label content',
  placeholder: 'password',
};

const meta = {
  title: 'Components/InputPassword',
  component: InputPassword,
  tags: ['autodocs'],
  decorators: [withFormik],
} satisfies Meta<typeof InputPassword>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args,
};
