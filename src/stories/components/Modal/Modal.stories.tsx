import type { Meta, StoryObj } from '@storybook/react';

import { IModalBase, Modal } from '../../../components/Modal';

const args: IModalBase = {
  show: true,
  position: 'center',
  title: 'Title',
  overlay: true,
  content: 'Call from User User',
  accept: () => null,
  reject: () => null,
  acceptButton: 'Accept',
  rejectButton: 'Reject',
};

const meta = {
  title: 'Components/Modal',
  component: Modal,
  tags: ['autodocs'],
} satisfies Meta<typeof Modal>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Call: Story = {
  args: {
    ...args,
    type: 'call',
  },
};
