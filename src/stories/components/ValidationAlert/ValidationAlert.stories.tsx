import type { Meta, StoryObj } from '@storybook/react';
import { ReactComponent as ErrorIcon } from 'assets/icons/common/error.svg';
import { ReactComponent as WarningIcon } from 'assets/icons/common/warning.svg';
import { ReactComponent as SuccessIcon } from 'assets/icons/common/success.svg';

import { ValidationAlert } from '../../../components/ValidationAlert/ValidationAlert';

const args = {
  message: 'Some message',
};

const meta = {
  title: 'Components/ValidationAlert',
  component: ValidationAlert,
  tags: ['autodocs'],
} satisfies Meta<typeof ValidationAlert>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Error: Story = {
  args: {
    ...args,
    type: 'error',
    icon: <ErrorIcon title="error" />,
  },
};

export const Warning: Story = {
  args: {
    ...args,
    type: 'warning',
    icon: <WarningIcon title="warning" />,
  },
};

export const Success: Story = {
  args: {
    ...args,
    type: 'success',
    icon: <SuccessIcon title="success" />,
  },
};
