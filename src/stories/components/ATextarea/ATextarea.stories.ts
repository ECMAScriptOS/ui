import withFormik from '@bbbtech/storybook-formik';
import type { Meta, StoryObj } from '@storybook/react';

import { ATextarea, IATextarea } from '../../../components/ATextarea';

const args: IATextarea = {
  id: 'inputTextID',
  name: 'name_1',
  label: 'label content',
  placeholder: 'placeholder',
};

const meta = {
  title: 'Components/ATextarea',
  component: ATextarea,
  tags: ['autodocs'],
  decorators: [withFormik],
} satisfies Meta<typeof ATextarea>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    ...args,
    disabled: false,
  },
};

export const Disabled: Story = {
  args: {
    ...args,
    name: 'name_3',
    disabled: true,
  },
};

export const WithoutLabel = {
  args: {
    ...args,
    name: 'name_3',
    label: undefined,
  },
};
