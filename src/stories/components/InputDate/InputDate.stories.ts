import withFormik from '@bbbtech/storybook-formik';
import type { Meta, StoryObj } from '@storybook/react';

import { IInputDate } from '../../../components/InputDate/type';
import { InputDate } from '../../../components/InputDate/InputDate';

const args: IInputDate = {
  id: 'inputDateID',
  name: 'name_1',
  label: 'label content',
  showDatePicker: (isShown) => null,
};

const meta = {
  title: 'Components/InputDate',
  component: InputDate,
  tags: ['autodocs'],
  decorators: [withFormik],
} satisfies Meta<typeof InputDate>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args,
};
