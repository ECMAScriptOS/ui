import withFormik from '@bbbtech/storybook-formik';
import type { Meta, StoryObj } from '@storybook/react';

import { ASelect } from '../../../components/ASelect/ASelect';
import { IASelect } from '../../../components/ASelect/type';

const args: IASelect = {
  id: 'selectID',
  name: 'name_1',
  placeholder: 'Placeholder',
  options: [
    { label: 'label_1', value: 'value_1' },
    { label: 'label_2', value: 'value_2' },
    { label: 'label_3', value: 'value_3' },
    { label: 'label_4', value: 'value_4' },
  ],
};

const meta = {
  title: 'Components/ASelect',
  component: ASelect,
  tags: ['autodocs'],
  decorators: [withFormik],
} satisfies Meta<typeof ASelect>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args,
};
