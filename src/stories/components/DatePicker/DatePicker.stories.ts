import type { Meta, StoryObj } from '@storybook/react';

import {
  labelDatePicker,
  months,
} from '../../../components/DatePicker/datePicker.mock';
import { DatePicker } from '../../../components/DatePicker/DatePicker';
import { IDatePicker } from '../../../components/DatePicker/type';

const args: IDatePicker = {
  coordinates: { x: 80, y: 80 },
  setIsVisible: (...args) => console.log(args),
  handleSubmit: (...args) => console.log(args),
  defaultValue: Date.now(),
  months,
  label: labelDatePicker,
};

const meta = {
  title: 'Components/DatePicker',
  component: DatePicker,
  tags: ['autodocs'],
} satisfies Meta<typeof DatePicker>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args,
};
