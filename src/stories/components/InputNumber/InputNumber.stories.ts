import withFormik from '@bbbtech/storybook-formik';
import type { Meta, StoryObj } from '@storybook/react';

import { InputNumber } from '../../../components/InputNumber/InputNumber';
import { IInputNumber } from '../../../components/InputNumber/type';

const args: IInputNumber = {
  id: 'inputNumberID',
  name: 'name_1',
  label: 'Some label',
  placeholder: 'Placeholder',
  min: 1,
  max: 10,
};

const meta = {
  title: 'Components/InputNumber',
  component: InputNumber,
  tags: ['autodocs'],
  decorators: [withFormik],
} satisfies Meta<typeof InputNumber>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args,
};
