import withFormik from '@bbbtech/storybook-formik';
import type { Meta, StoryObj } from '@storybook/react';

import { InputText } from '../../../components/InputText/InputText';
import { IInputText } from '../../../components/InputText/type';

const args: IInputText = {
  id: 'inputTextID',
  name: 'name_1',
  label: 'label content',
  placeholder: 'placeholder',
};

const meta = {
  title: 'Components/InputText',
  component: InputText,
  tags: ['autodocs'],
  decorators: [withFormik],
} satisfies Meta<typeof InputText>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    ...args,
    disabled: false,
  },
};

export const Disabled: Story = {
  args: {
    ...args,
    name: 'name_3',
    disabled: true,
  },
};
