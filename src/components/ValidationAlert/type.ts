import { ReactNode } from 'react';

export type ValidationAlertType = 'error' | 'warning' | 'success';

export interface IValidationAlert {
  type?: ValidationAlertType;
  /**
   * Content of alert
   */
  message: string;
  /**
   * Visible icon for alert message
   */
  icon?: ReactNode;
  /**
   * For customising component
   */
  className?: string;
}
