import { FC } from 'react';

import { AIcon } from '../AIcon/AIcon';
import { IValidationAlert } from './type';
import {
  StyledBackground,
  StyledValidationMessage,
  StyledWrapper,
} from './ValidationAlert.styles';

export const ValidationAlert: FC<IValidationAlert> = ({
  message,
  type = 'error',
  icon,
  className,
}) => (
  <StyledWrapper className={className} data-testid="ValidationAlert">
    <StyledBackground data-testid="ValidationAlert_background" type={type} />
    <StyledValidationMessage>{message}</StyledValidationMessage>
    {icon && <AIcon>{icon}</AIcon>}
  </StyledWrapper>
);
