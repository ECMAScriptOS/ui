import styled from 'styled-components';

import { COLORS, FONT_COLORS } from '../../styles/colors';
import { PADDINGS } from '../../styles/metrics';
import { ValidationAlertType } from './type';

export const StyledWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: ${PADDINGS.vertical_sm} ${PADDINGS.horizontal_sm};
  position: relative;
  color: ${FONT_COLORS.primary};
`;

export const StyledBackground = styled.div<{ type: ValidationAlertType }>`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  background-color: ${({ type }) => COLORS[type]};
  opacity: 0.15;
`;

export const StyledValidationMessage = styled.strong`
  font-size: 0.55rem;
`;
