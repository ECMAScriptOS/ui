import { cleanup, render, screen } from '@testing-library/react';
import { ReactComponent as ErrorIcon } from 'assets/icons/common/error.svg';

import { COLORS } from '../../../styles/colors';
import { IValidationAlert } from '../type';
import { ValidationAlert } from '../ValidationAlert';

const { error, warning, success } = COLORS;

describe('__component: ValidationAlert', () => {
  const props: IValidationAlert = {
    message: 'Some message',
  };

  afterEach(cleanup);

  it('should render correctly', () => {
    render(<ValidationAlert {...props} />);

    const componentValidationAlert = screen.getByTestId('ValidationAlert');

    expect(componentValidationAlert).toBeInTheDocument();
  });

  it('should handle property "message" correctly', () => {
    render(<ValidationAlert {...props} />);

    const validationMessageEl = screen.getByText(props.message);

    expect(validationMessageEl).toBeInTheDocument();
  });

  it('should handle property "icon" correctly', () => {
    render(<ValidationAlert {...props} icon={<ErrorIcon title="error" />} />);

    const errorIconEl = screen.getByTitle('error');

    expect(errorIconEl).toBeInTheDocument();
  });

  it('should handle property "type=error" correctly', () => {
    render(<ValidationAlert {...props} icon={<ErrorIcon title="error" />} />);

    const backgroundEl = screen.getByTestId('ValidationAlert_background');

    expect(backgroundEl).toHaveStyle({
      backgroundColor: error,
    });
  });

  it('should handle property "type=warning" correctly', () => {
    render(
      <ValidationAlert
        {...props}
        icon={<ErrorIcon title="error" />}
        type="warning"
      />
    );

    const backgroundEl = screen.getByTestId('ValidationAlert_background');

    expect(backgroundEl).toHaveStyle({
      backgroundColor: warning,
    });
  });

  it('should handle property "type=success" correctly', () => {
    render(
      <ValidationAlert
        {...props}
        icon={<ErrorIcon title="error" />}
        type="success"
      />
    );

    const backgroundEl = screen.getByTestId('ValidationAlert_background');

    expect(backgroundEl).toHaveStyle({
      backgroundColor: success,
    });
  });
});
