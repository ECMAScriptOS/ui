import styled from 'styled-components';

import { PositionType } from './type';
import { COLORS } from '../../styles/colors';

const positions: { [key in PositionType]: { [key: string]: string } } = {
  center: {
    top: '50%',
    left: '50%',
    transform: 'translate(-50%,-50%)',
  },
  top: {
    top: '0',
    left: '50%',
    transform: 'translateX(-50%)',
  },
  top_right: {
    top: '0',
    right: '0',
  },
  bottom_right: {
    bottom: '0',
    right: '0',
  },
};

export const StyledWrapper = styled.div<{ position: PositionType }>`
  position: fixed;
  ${({ position }) => positions[position]}
`;

export const StyledOverlay = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  opacity: 0.15;
  background-color: ${COLORS.primary};
`;
