import { FC, ReactElement } from 'react';
import ReactDOM from 'react-dom';

import { IModalBase } from './type';
import { CallModal } from './CallModal/CallModal';
import { StyledOverlay, StyledWrapper } from './Modal.styles';

export const Modal: FC<IModalBase> = ({
  show,
  type = 'call',
  position = 'center',
  overlay,
  ...rest
}) => {
  if (!show) return null;

  const modals: { [key in typeof type]: ReactElement } = {
    call: <CallModal {...rest} />,
  };

  return ReactDOM.createPortal(
    <>
      {overlay && <StyledOverlay data-testid="Overlay" />}
      <StyledWrapper data-testid="Modal" position={position}>
        {modals[type]}
      </StyledWrapper>
    </>,
    document.body
  );
};
