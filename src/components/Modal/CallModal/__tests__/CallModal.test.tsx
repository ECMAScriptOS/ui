import { cleanup, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { IModal } from '../../type';
import { CallModal } from '../CallModal';

describe('__component: CallModal', () => {
  const props: IModal = {
    title: 'Title',
    content: 'Call from User User',
    accept: jest.fn(),
    reject: jest.fn(),
    acceptButton: 'Accept',
    rejectButton: 'Reject',
  };

  const user = userEvent.setup();

  afterEach(cleanup);

  it('should render correctly', () => {
    render(<CallModal {...props} />);

    const componentCallModal = screen.getByTestId('CallModal');
    const acceptButton = screen.getByRole('button', {
      name: props.acceptButton,
    });
    const rejectButton = screen.getByRole('button', {
      name: props.rejectButton,
    });

    expect(componentCallModal).toBeInTheDocument();
    expect(rejectButton).toBeInTheDocument();
    expect(acceptButton).toBeInTheDocument();
  });

  it('should handle property "title" correctly', () => {
    render(<CallModal {...props} />);

    const titleEl = screen.getByRole('heading', { level: 3 });

    expect(titleEl).toHaveTextContent(props.title as string);
  });

  it('should handle property "content" correctly', () => {
    render(<CallModal {...props} />);

    const contentEl = screen.getByRole('paragraph');

    expect(contentEl).toHaveTextContent(props.content);
  });

  it('should handle property "accept" correctly', async () => {
    render(<CallModal {...props} />);

    const acceptButton = screen.getByRole('button', {
      name: props.acceptButton,
    });

    await user.click(acceptButton);

    expect(props.accept).toHaveBeenCalledTimes(1);
  });

  it('should handle property "reject" correctly', async () => {
    render(<CallModal {...props} />);

    const rejectButton = screen.getByRole('button', {
      name: props.rejectButton,
    });

    await user.click(rejectButton);

    expect(props.reject).toHaveBeenCalledTimes(1);
  });
});
