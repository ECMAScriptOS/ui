import styled from 'styled-components';

import { COLORS } from '../../../styles/colors';
import { PADDINGS } from '../../../styles/metrics';

const { primary, white } = COLORS;

const { vertical, horizontal } = PADDINGS;

export const StyledWrapper = styled.div`
  border: 1px solid ${primary};

  background-color: ${white};
`;

export const StyledHeader = styled.div`
  padding: ${vertical} ${horizontal};
  border-bottom: 1px solid ${primary};
`;

export const StyledBody = styled.div`
  display: flex;
  align-items: center;
  padding: 0 ${horizontal};
`;

export const StyledIcon = styled.div`
  padding: ${vertical} 0;

  & svg {
    width: 50px;
    height: 50px;
  }
`;

export const StyledActionSection = styled.div`
  display: flex;
  column-gap: 10px;
  justify-content: right;

  padding-right: ${horizontal};
  padding-bottom: ${vertical};
`;
