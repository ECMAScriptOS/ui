import { FC } from 'react';
import { ReactComponent as PhoneIcon } from 'assets/icons/common/phone.svg';

import {
  StyledActionSection,
  StyledBody,
  StyledHeader,
  StyledIcon,
  StyledWrapper,
} from './CallModal.styles';
import { AButton } from '../../AButton';
import { IModal } from '../type';

export const CallModal: FC<IModal> = ({
  title,
  content,
  accept,
  reject,
  acceptButton,
  rejectButton,
}) => {
  return (
    <StyledWrapper data-testid="CallModal">
      {title && (
        <StyledHeader>
          <h3>{title}</h3>
        </StyledHeader>
      )}
      <StyledBody>
        <StyledIcon>
          <PhoneIcon title="phone" />
        </StyledIcon>
        <p>{content}</p>
      </StyledBody>
      <StyledActionSection>
        <AButton handleClick={accept} color="primary">
          {acceptButton}
        </AButton>
        <AButton handleClick={reject} color="error">
          {rejectButton}
        </AButton>
      </StyledActionSection>
    </StyledWrapper>
  );
};
