export type PositionType = 'center' | 'top' | 'top_right' | 'bottom_right';
export type ModalType = 'call';

export interface IModalBase extends IModal {
  /**
   * Show modal if true
   */
  show: boolean;
  /**
   * Modal component type.
   */
  type?: ModalType;
  /**
   * Position on the Screen
   */
  position?: PositionType;
  /**
   * Show or hide overlay
   */
  overlay?: boolean;
}

export interface IModal {
  /**
   * Title content
   */
  title?: string;
  /**
   * Main content
   */
  content: string;
  /**
   * Accept handler
   */
  accept: () => void;
  /**
   * Reject handler
   */
  reject: () => void;
  /**
   * Accept button content
   */
  acceptButton: string;
  /**
   * Reject button content
   */
  rejectButton: string;
}
