import { cleanup, render, screen } from '@testing-library/react';

import { IModalBase } from '../type';
import { Modal } from '../Modal';

describe('__component: Modal', () => {
  const props: IModalBase = {
    show: true,
    position: 'center',
    title: 'Title',
    overlay: true,
    content: 'Call from User User',
    accept: jest.fn(),
    reject: jest.fn(),
    acceptButton: 'Accept',
    rejectButton: 'Reject',
  };

  afterEach(cleanup);

  it('should render correctly', () => {
    render(<Modal {...props} />);

    const componentModal = screen.getByTestId('Modal');
    const overlayEl = screen.getByTestId('Overlay');

    expect(componentModal).toBeInTheDocument();
    expect(overlayEl).toBeInTheDocument();
  });

  it('should handle property "show=false" correctly', () => {
    render(<Modal {...props} show={false} />);

    const componentModal = screen.queryByTestId('Modal');

    expect(componentModal).not.toBeInTheDocument();
  });

  it('should handle property "type=call" correctly', () => {
    render(<Modal {...props} type="call" />);

    const componentModal = screen.getByTestId('Modal');
    const componentCallModal = screen.getByTestId('CallModal');

    expect(componentModal).toBeInTheDocument();
    expect(componentCallModal).toBeInTheDocument();
  });

  it('should handle property "overlay=false" correctly', () => {
    render(<Modal {...props} overlay={false} />);

    const componentModal = screen.getByTestId('Modal');
    const overlayEl = screen.queryByTestId('Overlay');

    expect(componentModal).toBeInTheDocument();
    expect(overlayEl).not.toBeInTheDocument();
  });

  it('should handle property "position=center" correctly', () => {
    render(<Modal {...props} position="center" />);

    const componentModal = screen.getByTestId('Modal');

    expect(componentModal).toHaveStyle({
      top: '50%',
      left: '50%',
      transform: 'translate(-50%,-50%)',
    });
  });

  it('should handle property "position=top" correctly', () => {
    render(<Modal {...props} position="top" />);

    const componentModal = screen.getByTestId('Modal');

    expect(componentModal).toHaveStyle({
      top: '0',
      left: '50%',
      transform: 'translateX(-50%)',
    });
  });

  it('should handle property "position=top_right" correctly', () => {
    render(<Modal {...props} position="top_right" />);

    const componentModal = screen.getByTestId('Modal');

    expect(componentModal).toHaveStyle({
      top: '0',
      right: '0',
    });
  });

  it('should handle property "position=bottom_right" correctly', () => {
    render(<Modal {...props} position="bottom_right" />);

    const componentModal = screen.getByTestId('Modal');

    expect(componentModal).toHaveStyle({
      bottom: '0',
      right: '0',
    });
  });
});
