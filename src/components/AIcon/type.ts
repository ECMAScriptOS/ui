import { ReactNode } from 'react';

import { ColorType } from '../../types';

export type AIconSizeType = 'extraSmall' | 'normal';

export interface IAIcon {
  /**
   * Render icon
   */
  children: ReactNode;
  /**
   * className attribute for customising styled-component
   */
  className?: string;
  /**
   * Icon color
   */
  color?: ColorType;
  /**
   * Icon size
   */
  size?: AIconSizeType;
}
