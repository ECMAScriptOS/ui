import { FC } from 'react';

import { StyledWrapper } from './AIcon.styles';
import { IAIcon } from './type';

export const AIcon: FC<IAIcon> = ({
  children,
  className,
  color = 'primary',
  size = 'normal',
}) => (
  <StyledWrapper
    size={size}
    color={color}
    className={className}
    data-testid="AIcon"
  >
    {children}
  </StyledWrapper>
);
