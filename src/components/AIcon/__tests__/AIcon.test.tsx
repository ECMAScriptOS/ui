import { cleanup, render, screen } from '@testing-library/react';

import { AIcon } from '../AIcon';

describe('__component: AIcon', () => {
  const children = <div>content</div>;

  afterEach(cleanup);

  it('should render correctly', () => {
    render(<AIcon>{children}</AIcon>);

    const component = screen.getByTestId('AIcon');

    expect(component).toBeInTheDocument();
  });
});
