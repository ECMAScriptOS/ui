import styled from 'styled-components';

import { ColorType } from '../../types';
import { COLORS } from '../../styles/colors';
import { FlexCenteredXY } from '../../styles/common';
import { AIconSizeType } from './type';

const ICON_SIZES: { [key in AIconSizeType]: { [key: string]: string } } = {
  extraSmall: { width: '10px', height: 'auto' },
  normal: { width: '20px', height: 'auto' },
};

export const StyledWrapper = styled.div<{
  color: ColorType;
  size: AIconSizeType;
}>`
  ${FlexCenteredXY};

  & svg {
    stroke: ${({ color }) => COLORS[color]};
    fill: ${({ color }) => COLORS[color]};
    ${({ size }) => ICON_SIZES[size]};
  }
`;
