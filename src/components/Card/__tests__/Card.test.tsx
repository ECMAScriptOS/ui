import { cleanup, screen } from '@testing-library/react';

import { renderWithRouter } from '../../../utils/testDecorators';
import { Card } from '../Card';
import { ICard } from '../type';

describe('__component: Card', () => {
  const props: ICard = {
    to: '/test',
    $backgroundImage: './test-url',
    title: 'title',
  };

  afterEach(cleanup);

  it('should render correctly', () => {
    renderWithRouter(<Card {...props} />);

    const cardComponent = screen.getByTestId('Card');

    expect(cardComponent).toBeInTheDocument();
  });

  it('should handle property "to" correctly', () => {
    renderWithRouter(<Card {...props} />);

    const linkEl = screen.getByRole('link');

    expect(linkEl).toBeInTheDocument();
    expect(linkEl).toHaveAttribute('href', props.to);
  });

  it('should handle property "$backgroundImage" correctly', () => {
    renderWithRouter(<Card {...props} />);

    const cardComponent = screen.getByTestId('Card');

    expect(cardComponent).toHaveStyle(
      `background-image: url(${props.$backgroundImage})`
    );
  });

  it('should handle property "title" correctly', () => {
    renderWithRouter(<Card {...props} />);

    const headingEl = screen.getByRole('heading', { level: 2 });

    expect(headingEl).toHaveTextContent(props.title);
  });
});
