import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { PADDINGS } from '../../styles/metrics';
import { FONT_COLORS } from '../../styles/colors';

const { vertical, horizontal } = PADDINGS;
export const StyledWrapper = styled(Link)<{ $backgroundImage: string }>`
  display: block;

  padding: ${vertical} 0px;
  color: ${FONT_COLORS.white};
  text-decoration: none;

  background-image: url(${({ $backgroundImage }) => $backgroundImage});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;

  &:hover > h2 {
    width: 100%;
  }
`;

export const StyledTitle = styled.h2`
  padding: ${vertical} ${horizontal};
  width: 70%;

  transition: width ease-out 0.1s;

  backdrop-filter: invert(30%);
`;
