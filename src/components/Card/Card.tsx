import { FC } from 'react';

import { ICard } from './type';
import { StyledTitle, StyledWrapper } from './Card.styles';

export const Card: FC<ICard> = ({ title, $backgroundImage, to }) => (
  <StyledWrapper data-testid="Card" to={to} $backgroundImage={$backgroundImage}>
    <StyledTitle>{title}</StyledTitle>
  </StyledWrapper>
);
