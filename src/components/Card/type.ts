export interface ICard {
  to: string;
  title: string;
  $backgroundImage: string;
}
