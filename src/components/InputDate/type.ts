import { RefObject } from 'react';

export interface IInputDate {
  /**
   * Input id attribute
   */
  id: string;
  /**
   * Name of input field
   */
  name: string;
  /**
   * If true input cannot be focused.
   */
  disabled?: boolean;
  /**
   * Label content.
   */
  label?: string;
  /**
   * Handler for showing DatePicker component
   */
  showDatePicker: (isShown: boolean) => void;
  /**
   *
   */
  innerRef?: RefObject<HTMLDivElement>;
}
