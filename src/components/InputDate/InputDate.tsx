import { FC, HTMLAttributes } from 'react';
import { Field, FieldProps } from 'formik';
import { ReactComponent as CalendarIcon } from 'assets/icons/common/calendar.svg';

import { IInputDate } from './type';
import {
  StyledAButton,
  StyledInput,
  StyledInputWrapper,
  StyledLabel,
  StyledValidationAlert,
  StyledWrapper,
} from './InputDate.styles';

// ToDo: Find way to follow locale input type date
// ToDo: 0035 transform to 1935
export const InputDate: FC<IInputDate & HTMLAttributes<HTMLInputElement>> = ({
  id,
  name,
  disabled,
  label,
  showDatePicker,
  innerRef,
  className,
  ...rest
}) => (
  <Field name={name}>
    {({ field, meta }: FieldProps) => (
      <StyledWrapper
        ref={innerRef}
        data-testid="InputDate"
        className={className}
      >
        {label && (
          <StyledLabel disabled={disabled} htmlFor={id}>
            {label}
          </StyledLabel>
        )}
        <StyledInputWrapper>
          <StyledInput
            type="date"
            role="date"
            id={id}
            isValid={!meta.touched || !meta.error}
            disabled={disabled}
            {...field}
            {...rest}
          />
          <StyledAButton
            icon={<CalendarIcon title="calendar" />}
            handleClick={() => showDatePicker(true)}
            rounded
          />
        </StyledInputWrapper>
        {meta.touched && meta.error && (
          <StyledValidationAlert message={meta.error} />
        )}
      </StyledWrapper>
    )}
  </Field>
);
