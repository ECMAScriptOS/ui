import { cleanup, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { COLORS, FONT_COLORS } from '../../../styles/colors';
import { withFormik } from '../../../utils/testDecorators';
import { InputDate } from '../InputDate';
import { IInputDate } from '../type';

const { gray } = COLORS;

describe('__component: InputDate', () => {
  const props: IInputDate = {
    id: 'inputDateID',
    name: 'inputDateName',
    disabled: false,
    showDatePicker: jest.fn(),
  };
  const label = 'label';
  const mockSubmit = jest.fn();
  const user = userEvent.setup();

  const getComponentWithFormik = withFormik(InputDate, props);

  afterEach(cleanup);

  it('should render correctly', () => {
    render(getComponentWithFormik(mockSubmit));

    const componentAInput = screen.getByTestId('InputDate');

    expect(componentAInput).toBeInTheDocument();
  });

  it('should not be focused with  "disabled=true" property', () => {
    render(getComponentWithFormik(mockSubmit, { label, disabled: true }));

    const inputEl = screen.getByRole('date');
    const labelEl = screen.getByText(label);

    user.click(inputEl);
    expect(inputEl).not.toHaveFocus();

    expect(inputEl).toHaveStyle({ borderBottomColor: gray });
    expect(labelEl).toHaveStyle({ color: FONT_COLORS.gray });
  });

  it('should handle "label" property correctly', () => {
    const label = 'label';
    render(getComponentWithFormik(mockSubmit, { label }));

    const element = screen.getByText(label);

    expect(element.tagName).toBe('LABEL');
  });

  it('should handle "id" property correctly', () => {
    const label = 'label';
    render(getComponentWithFormik(mockSubmit, { label }));

    const inputEl = screen.getByRole('date');
    const labelEl = screen.getByText(label);

    expect(inputEl).toHaveAttribute('id', props.id);
    expect(labelEl).toHaveAttribute('for', props.id);
  });

  it('should handle "showDatePicker" property correctly', async () => {
    render(getComponentWithFormik(mockSubmit));

    const buttonShowDatePickerEl = screen.getByRole('button');

    await user.click(buttonShowDatePickerEl);

    expect(props.showDatePicker).toBeCalledWith(true);
  });
});
