import { ReactNode } from 'react';

export type ALinkType = 'normal';

export interface IALink {
  children: string | ReactNode;
  /**
   * 'href' attribute
   */
  to: string;
  type?: ALinkType;
  /**
   * className attribute for customising styled-component
   */
  className?: string;
  /**
   * For unit-tests only
   */
  dataTestId?: string;
}
