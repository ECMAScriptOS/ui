import { FC } from 'react';

import { StyledLink } from './ALink.styles';
import { IALink } from './type';

export const ALink: FC<IALink> = ({
  dataTestId = 'ALink',
  children,
  to,
  type = 'normal',
  className,
}) => (
  <StyledLink
    data-testid={dataTestId}
    type={type}
    to={to}
    className={className}
  >
    {children}
  </StyledLink>
);
