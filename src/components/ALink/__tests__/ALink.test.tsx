import { cleanup, screen } from '@testing-library/react';

import { renderWithRouter } from '../../../utils/testDecorators';
import { ALink } from '../ALink';

describe('__component: ALink', () => {
  const to = '/test-path';

  afterEach(cleanup);

  it('should render correctly', () => {
    renderWithRouter(<ALink to={to}>Test</ALink>);

    const linkEl = screen.getByTestId('ALink');

    expect(linkEl).toBeInTheDocument();
    expect(linkEl).toHaveAttribute('href', to);
  });
});
