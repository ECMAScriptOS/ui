import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { FONT_COLORS } from '../../styles/colors';
import { ALinkType } from './type';

const LINK_TYPES: { [key in ALinkType]: { [key: string]: string } } = {
  normal: {
    fontSize: '1rem',
  },
};

export const StyledLink = styled(Link)<{ type: ALinkType }>`
  display: inline-block;
  text-transform: uppercase;
  text-decoration: none;
  color: ${FONT_COLORS.primary};
  font-size: 1rem;

  ${({ type }) => LINK_TYPES[type]};

  &:active {
    color: ${FONT_COLORS.gray};
  }

  &:hover {
    text-decoration: underline;
  }
`;
