import { FC, useState, MouseEvent } from 'react';

import { StyledWrapper, StyledRippleElement } from './AButton.styles';
import { IAButton } from './type';
import { AIcon } from '../AIcon';

export const AButton: FC<IAButton> = ({
  children,
  color = 'primary',
  icon,
  type = 'button',
  handleClick,
  disabled,
  filled,
  rounded,
  className,
}) => {
  const [clickCoordinates, setClickCoordinates] = useState({ x: 0, y: 0 });
  const [isHorizontal, setIsHorizontal] = useState(true);

  const handleClickWithRippleEffect = (
    event: MouseEvent<HTMLButtonElement>
  ) => {
    const rippleContainer = event.currentTarget.getBoundingClientRect();

    if (rippleContainer.width < rippleContainer.height) setIsHorizontal(false);

    const size = Math.max(rippleContainer.width, rippleContainer.height);

    const x = event.clientX - rippleContainer.left - size / 2;
    const y = event.clientY - rippleContainer.top - size / 2;

    setClickCoordinates({ x, y });
  };

  return (
    <StyledWrapper
      className={className}
      color={color}
      isIconType={!!icon}
      onMouseDown={icon ? handleClickWithRippleEffect : undefined}
      filled={filled}
      rounded={rounded}
      onClick={handleClick}
      type={type}
      disabled={disabled}
      data-testid="AButton"
    >
      {icon ? (
        <>
          <StyledRippleElement
            data-testid="RippleElement"
            isHorizontal={isHorizontal}
            {...clickCoordinates}
          />
          <AIcon color={color}>{icon}</AIcon>
        </>
      ) : (
        children
      )}
    </StyledWrapper>
  );
};
