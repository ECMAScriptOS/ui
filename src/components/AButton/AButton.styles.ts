import styled, { css, keyframes } from 'styled-components';

import { COLORS } from '../../styles/colors';
import { FlexCenteredXY } from '../../styles/common';
import { PADDINGS } from '../../styles/metrics';
import { hexToRgba } from '../../utils/hexToRgba';
import { ColorType } from '../../types';

const { vertical, horizontal } = PADDINGS;
const { white } = COLORS;

interface IButton {
  color: ColorType;
  filled?: boolean;
  rounded?: boolean;
  isIconType: boolean;
}

const ripple = keyframes`
    to {
        transform: scale(4);
        opacity: 0;
    }
`;

const AButtonStyles = css`
  text-transform: uppercase;
  padding: ${vertical} ${horizontal};

  ${({ filled, color }: IButton) =>
    `
      background-color: ${filled ? COLORS[color] : white};
      color:  ${filled ? white : COLORS[color]};
      border: 1px solid ${COLORS[color]};
      
      &:active:not(:disabled) {
        background-color: ${hexToRgba(COLORS[color], filled ? 0.8 : 0.2)};
        transition-duration: 0.1s;
      }
    `};
`;

const AButtonIconStyles = css`
  padding: ${vertical};
  overflow: hidden;

  ${({ filled, color, rounded }: IButton) =>
    `
      background-color: ${filled ? COLORS[color] : 'transparent'};
          
      & {
        border-radius: ${rounded && '50%'};
      }
      
      & > div > svg {
        stroke: ${filled && white};
        fill: ${filled && white};
      }
  
      &:hover:not(:disabled) {
        box-shadow: 0px 0px 9px -1px ${filled && hexToRgba(COLORS[color])};
        background-color: ${!filled && hexToRgba(COLORS[color], 0.1)};
        transition-duration: 0.2s;
      }

      & > div:first-child {
        background-color: ${filled ? white : hexToRgba(COLORS[color], 0.3)};
      }
    `};

  &:active:not(:disabled) {
    box-shadow: none;
    transform: scale(0.96);
    transition-duration: 0.2s;
  }

  &:active:not(:disabled) > div:first-child {
    animation: ${ripple} 1.4s cubic-bezier(0.23, 1, 0.32, 1);
  }
`;

export const StyledWrapper = styled.button<IButton>`
  position: relative;
  ${FlexCenteredXY};

  &:disabled {
    cursor: default;
  }

  ${({ isIconType }) => (isIconType ? AButtonIconStyles : AButtonStyles)};
`;

export const StyledRippleElement = styled.div<{
  x: number;
  y: number;
  isHorizontal: boolean;
}>`
  position: absolute;
  top: ${({ y }) => y}px;
  left: ${({ x }) => x}px;

  aspect-ratio: 1 / 1;
  ${({ isHorizontal }) => (isHorizontal ? 'width: 100%;' : 'height: 100%;')};
  transform: scale(0);
  opacity: 0.6;
  border-radius: 50%;
`;
