import { cleanup, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ReactComponent as ErrorIcon } from 'assets/icons/common/error.svg';

import { AButton } from '../AButton';

describe('__component: AButton', () => {
  const children = 'Test content';
  const user = userEvent.setup();

  afterEach(cleanup);

  it('should render correctly', () => {
    render(<AButton>{children}</AButton>);

    const component = screen.getByTestId('AButton');

    const rippleElement = screen.queryByTestId('RippleElement');

    expect(rippleElement).not.toBeInTheDocument();
    expect(component).toBeInTheDocument();
  });

  it('should handle children property correctly', () => {
    render(<AButton>{children}</AButton>);

    const component = screen.getByTestId('AButton');

    expect(component).toHaveTextContent(children);
  });

  it('should handle property icon correctly', () => {
    render(<AButton icon={<ErrorIcon title="error" />} />);

    const rippleElement = screen.getByTestId('RippleElement');
    const svgElement = screen.getByTitle('error');

    expect(rippleElement).toBeInTheDocument();
    expect(svgElement).toBeInTheDocument();
    expect(svgElement.nodeName).toBe('svg');
  });

  it('should handle property handleClick correctly', async () => {
    const mockOnClick = jest.fn();

    render(<AButton handleClick={mockOnClick}>{children}</AButton>);

    const component = screen.getByTestId('AButton');

    await user.click(component);
    await user.click(component);

    expect(mockOnClick).toHaveBeenCalledTimes(2);
  });

  it('should handle property disabled=true correctly', async () => {
    const mockOnClick = jest.fn();

    render(
      <AButton disabled={true} handleClick={mockOnClick}>
        {children}
      </AButton>
    );

    const component = screen.getByTestId('AButton');

    await user.click(component);
    await user.click(component);

    expect(mockOnClick).not.toHaveBeenCalled();
  });
});
