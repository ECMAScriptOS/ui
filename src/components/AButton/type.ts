import { ReactNode } from 'react';

import { ColorType } from '../../types';

export interface IAButton {
  /**
   * Content to render UI
   */
  children?: ReactNode | string;
  /**
   * Svg icon
   */
  icon?: ReactNode;
  /**
   * HTML type attribute
   */
  type?: 'button' | 'submit';
  /**
   * Function for click event
   */
  handleClick?: () => void;
  /**
   * For customising styled component
   */
  className?: string;
  /**
   * Property for disabling button's click event
   */
  disabled?: boolean;
  /**
   * Component color
   */
  color?: ColorType;
  /**
   * Button shape rounded
   */
  rounded?: boolean;
  /**
   * Background color or transparent
   */
  filled?: boolean;
}
