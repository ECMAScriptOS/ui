import { cleanup, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { withFormik } from '../../../utils/testDecorators';
import { ASelect } from '../ASelect';
import { IASelect } from '../type';

describe('__component: ASelect', () => {
  const props: IASelect = {
    id: 'selectID',
    name: 'selectName',
    options: [
      { label: 'label 1', value: 'value 1' },
      { label: 'label 2', value: 'value 2' },
      { label: 'label 3', value: 'value 3' },
    ],
  };
  const mockSubmit = jest.fn();
  const user = userEvent.setup();

  const getComponentWithFormik = withFormik(ASelect, props);

  afterEach(cleanup);

  it('should render correctly', () => {
    render(getComponentWithFormik(mockSubmit));

    const component = screen.getByTestId('ASelect');

    expect(component).toBeInTheDocument();
  });

  it('should handle "options" property correctly', async () => {
    render(getComponentWithFormik(mockSubmit));

    const inputEL = screen.getByRole('textbox');
    let options = screen.queryByRole('list');

    expect(options).toBeNull();

    await user.click(inputEL);

    options = screen.getByRole('list');
    const optionElements = screen.getAllByRole('listitem');

    expect(options).toBeInTheDocument();

    expect(optionElements).toHaveLength(props.options.length);
  });

  it('should handle "id" property correctly', () => {
    const label = 'label';
    render(getComponentWithFormik(mockSubmit, { label }));

    const inputEL = screen.getByRole('textbox');
    const labelEL = screen.getByText(label);

    expect(inputEL).toHaveAttribute('id', props.id);
    expect(labelEL).toHaveAttribute('for', props.id);
  });

  it('should handle "placeholder" property correctly', () => {
    const placeholder = 'placeholder';
    render(getComponentWithFormik(mockSubmit, { placeholder }));

    const element = screen.getByPlaceholderText(placeholder);

    expect(element).toBeInTheDocument();
  });

  it('should handle "label" property correctly', () => {
    const label = 'label';
    render(getComponentWithFormik(mockSubmit, { label }));

    const element = screen.getByText(label);

    expect(element.tagName).toEqual('LABEL');
  });

  it('should handle change value event correctly', async () => {
    render(getComponentWithFormik(mockSubmit));

    const inputEl = screen.getByRole('textbox');

    await user.click(inputEl);

    const listItems = screen.getAllByRole('listitem');

    await user.click(listItems[1]);

    expect(inputEl).toHaveValue(props.options[1].value);
  });
});
