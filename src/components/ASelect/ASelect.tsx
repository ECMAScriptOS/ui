import { FC, useRef, useState } from 'react';
import { Field, FieldProps, useField } from 'formik';
import { ReactComponent as DownArrowIcon } from 'assets/icons/common/down.svg';

import { useClickOutsideDetector } from '../../hooks/useClickOutsideDetector';
import {
  StyledAButton,
  StyledInput,
  StyledInputValue,
  StyledLabel,
  StyledOption,
  StyledOptions,
  StyledSelectWrapper,
  StyledValidationAlert,
  StyledWrapper,
} from './ASelect.styles';
import { IASelect } from './type';

export const ASelect: FC<IASelect> = ({
  id,
  name,
  options,
  placeholder,
  label,
}) => {
  const [optionsIsVisible, setOptionsIsVisible] = useState(false);

  // ToDo:  There may be another way to get field props
  const { setValue } = useField(name)[2];

  const elementRef = useRef<HTMLDivElement>(null);

  useClickOutsideDetector(elementRef, setOptionsIsVisible);

  const optionsJSX = options.map((option) => (
    <StyledOption
      onClick={() => setValue(option.value)}
      key={option.value}
      value={option.value}
    >
      {option.label}
    </StyledOption>
  ));

  return (
    <Field name={name}>
      {({ field, meta }: FieldProps) => (
        <StyledWrapper data-testid="ASelect">
          {label && <StyledLabel htmlFor={id}>{label}</StyledLabel>}
          <StyledSelectWrapper
            onClick={() => setOptionsIsVisible((prev) => !prev)}
            ref={elementRef}
          >
            <StyledInput
              id={id}
              placeholder={placeholder}
              isValid={!meta.touched || !meta.error}
              readOnly
              {...field}
            />
            <StyledInputValue>
              {options.find(({ value }) => value === field.value)?.label}
            </StyledInputValue>
            <StyledAButton
              optionsIsVisible={optionsIsVisible}
              rounded
              icon={
                <DownArrowIcon
                  title={optionsIsVisible ? 'hide options' : 'show options'}
                />
              }
            />
            {optionsIsVisible && <StyledOptions>{optionsJSX}</StyledOptions>}
          </StyledSelectWrapper>
          {meta.touched && meta.error && (
            <StyledValidationAlert message={meta.error} />
          )}
        </StyledWrapper>
      )}
    </Field>
  );
};
