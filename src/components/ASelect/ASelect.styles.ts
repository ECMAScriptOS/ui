import styled from 'styled-components';

import { ValidationAlert } from '../ValidationAlert/ValidationAlert';
import { INPUT_PADDINGS } from '../../styles/metrics';
import { FlexColumnDirection } from '../../styles/common';
import { COLORS, FONT_COLORS } from '../../styles/colors';
import { AButton } from '../AButton';

const { vertical, left, right } = INPUT_PADDINGS;
const { primary, white, gray, error } = COLORS;

export const StyledWrapper = styled.div`
  position: relative;

  ${FlexColumnDirection};
`;

export const StyledLabel = styled.label`
  margin-left: 1.2rem;
`;

export const StyledSelectWrapper = styled.div`
  position: relative;
`;

export const StyledInput = styled.input<{ isValid: boolean }>`
  border-bottom: 1px solid ${({ isValid }) => (isValid ? primary : error)};
  width: 100%;
  padding: ${vertical} ${right} ${vertical} ${left};
  cursor: pointer;
  color: transparent;
`;

export const StyledInputValue = styled.p`
  position: absolute;
  top: 0;
  left: 0;
  padding: ${vertical} ${right} ${vertical} ${left};
  color: ${FONT_COLORS.primary};
`;

export const StyledAButton = styled(AButton)<{
  optionsIsVisible: boolean;
}>`
  position: absolute;
  top: 0;
  right: 4px;
  height: 100%;

  & svg {
    transform: rotate(
      ${({ optionsIsVisible }) => optionsIsVisible && '180deg'}
    );
  }
`;

export const StyledOptions = styled.ul`
  z-index: 1;
  position: absolute;
  top: 100%;
  width: 100%;
  border: 1px solid ${primary};
  background-color: ${white};
  max-height: 300px;
  overflow-y: auto;

  & :not(:last-child) {
    border-bottom: 1px solid ${primary};
  }
`;

export const StyledOption = styled.li`
  padding: ${vertical} ${right} ${vertical} ${left};
  cursor: pointer;

  &:hover {
    background-color: ${gray};
  }
`;

export const StyledValidationAlert = styled(ValidationAlert)`
  position: absolute;
  top: 100%;
  width: 100%;
`;
