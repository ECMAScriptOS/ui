export interface IASelect {
  /**
   * ASelect id attribute
   */
  id: string;
  /**
   * Name of select field
   */
  name: string;
  /**
   * List of available options
   */
  options: {
    label: string;
    value: number | string;
  }[];
  /**
   * Select form element placeholder
   */
  placeholder?: string;
  /**
   * Visible select label
   */
  label?: string;
}
