import { FC } from 'react';
import { Field, FieldProps } from 'formik';
import { ReactComponent as CheckedIcon } from 'assets/icons/common/checked.svg';

import {
  StyledCheckbox,
  StyledCheckboxWrapper,
  StyledCheckedIcon,
  StyledLabel,
  StyledWrapper,
} from './ACheckbox.styles';
import { IACheckbox } from './type';

export const ACheckbox: FC<IACheckbox> = ({
  id,
  label,
  disabled,
  labelPosition,
  className,
  name,
}) => (
  <Field type="checkbox" name={name}>
    {({ field }: FieldProps) => (
      <StyledWrapper
        className={className}
        data-testid="ACheckbox"
        labelPosition={labelPosition}
      >
        {label && (
          <StyledLabel disabled={disabled} htmlFor={id}>
            {label}
          </StyledLabel>
        )}
        <StyledCheckboxWrapper>
          <StyledCheckedIcon disabled={disabled} checked={field.value}>
            {field.value && <CheckedIcon title="checked" />}
          </StyledCheckedIcon>
          <StyledCheckbox
            id={id}
            disabled={disabled}
            type="checkbox"
            {...field}
          />
        </StyledCheckboxWrapper>
      </StyledWrapper>
    )}
  </Field>
);
