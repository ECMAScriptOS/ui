import styled from 'styled-components';

import { COLORS, FONT_COLORS } from '../../styles/colors';
import { FlexCenteredXY, getFlexDirection } from '../../styles/common';
import { LabelPositionType } from '../../types';
import { AIcon } from '../AIcon/AIcon';

const { primary, gray } = COLORS;

export const StyledWrapper = styled.div<{
  labelPosition?: LabelPositionType;
}>`
  ${FlexCenteredXY};
  flex-direction: ${({ labelPosition }) => getFlexDirection(labelPosition)};
  gap: 8px;
`;

export const StyledLabel = styled.label<{ disabled?: boolean }>`
  color: ${({ disabled }) =>
    disabled ? FONT_COLORS.gray : FONT_COLORS.primary};
  cursor: ${({ disabled }) => !disabled && 'pointer'};
`;

export const StyledCheckboxWrapper = styled.div`
  position: relative;
`;

export const StyledCheckedIcon = styled(AIcon)<{
  disabled?: boolean;
  checked?: boolean;
}>`
  width: 20px;
  height: 20px;
  background-color: ${({ checked }) => checked && primary};
  border: 1px solid ${({ disabled }) => (disabled ? gray : primary)};

  & svg {
    fill: ${COLORS.white};
  }
`;

export const StyledCheckbox = styled.input`
  position: absolute;
  top: 0;
  left: 0;
  width: 20px;
  height: 20px;
  opacity: 0;
  cursor: ${({ disabled }) => !disabled && 'pointer'};
`;
