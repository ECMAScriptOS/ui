import { cleanup, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { COLORS } from '../../../styles/colors';
import { withFormik } from '../../../utils/testDecorators';
import { ACheckbox } from '../ACheckbox';
import { IACheckbox } from '../type';

describe('__component: ACheckbox', () => {
  const props: IACheckbox = {
    id: 'checkboxID',
    name: 'checkboxName',
    disabled: false,
    labelPosition: 'right',
  };
  const label = 'label';
  const mockSubmit = jest.fn();
  const user = userEvent.setup();

  const getComponentWithFormik = withFormik(ACheckbox, props);

  afterEach(cleanup);

  it('should render correctly', () => {
    render(getComponentWithFormik(mockSubmit));

    const componentACheckbox = screen.getByTestId('ACheckbox');

    expect(componentACheckbox).toBeInTheDocument();
  });

  it('should handle "label" property correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label }));

    const element = screen.getByText(label);

    expect(element.tagName).toBe('LABEL');
  });

  it('should handle "id" property correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label }));

    const checkboxEl = screen.getByRole('checkbox');
    const labelEl = screen.getByText(label);

    expect(checkboxEl).toHaveAttribute('id', props.id);
    expect(labelEl).toHaveAttribute('for', props.id);
  });

  it('should handle "disabled" property correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label, disabled: true }));

    const checkboxEl = screen.getByRole('checkbox');
    const labelEl = screen.getByText(label);

    expect(checkboxEl).toBeDisabled();

    user.click(checkboxEl);

    expect(checkboxEl).not.toBeChecked();
    expect(labelEl).toHaveStyle({ color: COLORS.gray });
  });

  it('should handle "labelPosition=right" property correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label }));

    const componentACheckbox = screen.getByTestId('ACheckbox');

    expect(componentACheckbox).toHaveStyle({ flexDirection: 'row-reverse' });
  });

  it('should handle "labelPosition=top" property correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label, labelPosition: 'top' }));

    const componentACheckbox = screen.getByTestId('ACheckbox');

    expect(componentACheckbox).toHaveStyle({ flexDirection: 'column' });
  });

  it('should handle "labelPosition=bottom" property correctly', () => {
    render(
      getComponentWithFormik(mockSubmit, { label, labelPosition: 'bottom' })
    );

    const componentACheckbox = screen.getByTestId('ACheckbox');

    expect(componentACheckbox).toHaveStyle({ flexDirection: 'column-reverse' });
  });

  it('should handle "labelPosition=left" property correctly', () => {
    render(
      getComponentWithFormik(mockSubmit, { label, labelPosition: 'left' })
    );

    const componentACheckbox = screen.getByTestId('ACheckbox');

    expect(componentACheckbox).toHaveStyle({ flexDirection: 'row' });
  });

  it('should render svg-icon if checkbox is checked', async () => {
    render(getComponentWithFormik(mockSubmit, props));

    const checkboxEl = screen.getByRole('checkbox');

    await user.click(checkboxEl);

    const checkedIconEl = screen.getByTitle('checked');

    expect(checkedIconEl).toBeInTheDocument();
  });
});
