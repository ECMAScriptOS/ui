import { LabelPositionType } from '../../types';

export interface IACheckbox {
  /**
   * Input id attribute
   */
  id: string;
  /**
   * Content of checkbox
   */
  label?: string;
  /**
   * Name of checkbox
   */
  name: string;
  /**
   * If checkbox is disabled
   */
  disabled?: boolean;
  /**
   * Location for checkbox label
   */
  labelPosition?: LabelPositionType;
  /**
   * className attribute for customising styled-component
   */
  className?: string;
}
