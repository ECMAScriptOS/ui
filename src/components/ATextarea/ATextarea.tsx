import { FC, HTMLAttributes } from 'react';
import { Field, FieldProps } from 'formik';

import { IATextarea } from './type';
import {
  StyledLabel,
  StyledTextarea,
  StyledValidationAlert,
  StyledWrapper,
} from './ATextarea.styles';

export const ATextarea: FC<
  IATextarea & HTMLAttributes<HTMLTextAreaElement>
> = ({ id, name, label, placeholder, disabled, className, ...rest }) => (
  <Field name={name}>
    {({ field, meta }: FieldProps) => (
      <StyledWrapper data-testid="ATextarea" className={className}>
        {label && (
          <StyledLabel disabled={disabled} htmlFor={id}>
            {label}
          </StyledLabel>
        )}
        <StyledTextarea
          id={id}
          placeholder={placeholder}
          isValid={!meta.touched || !meta.error}
          disabled={disabled}
          {...field}
          {...rest}
        />
        {meta.touched && meta.error && (
          <StyledValidationAlert message={meta.error} />
        )}
      </StyledWrapper>
    )}
  </Field>
);
