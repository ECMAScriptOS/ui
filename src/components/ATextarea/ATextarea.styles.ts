import styled from 'styled-components';

import { ValidationAlert } from '../ValidationAlert';
import { COLORS, FONT_COLORS } from '../../styles/colors';
import { INPUT_PADDINGS } from '../../styles/metrics';
import { FlexColumnDirection } from '../../styles/common';

const { primary, error, gray } = COLORS;
const { vertical, left, right } = INPUT_PADDINGS;

export const StyledWrapper = styled.div`
  position: relative;

  ${FlexColumnDirection};
`;

export const StyledLabel = styled.label<{ disabled?: boolean }>`
  margin-left: 1.2rem;
  color: ${({ disabled }) => disabled && FONT_COLORS.gray};
`;

export const StyledTextarea = styled.textarea<{ isValid: boolean }>`
  outline: none;
  resize: none;
  border: 1px solid ${({ isValid }) => (isValid ? primary : error)};
  border-color: ${({ disabled }) => disabled && gray};
  padding: ${vertical} ${right} ${vertical} ${left};
  width: 100%;

  &::-webkit-scrollbar-track:hover {
    cursor: pointer;
  }

  &::-webkit-scrollbar-thumb:hover {
    cursor: pointer;
  }
`;

export const StyledValidationAlert = styled(ValidationAlert)`
  position: absolute;
  top: 100%;
  width: 100%;
`;
