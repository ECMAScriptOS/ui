import { cleanup, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { COLORS, FONT_COLORS } from '../../../styles/colors';
import { IATextarea } from '../type';
import { withFormik } from '../../../utils/testDecorators';
import { ATextarea } from '../ATextarea';

const { gray } = COLORS;

describe('__component: ATextarea', () => {
  const props: IATextarea = {
    id: 'inputTextID',
    name: 'inputDateName',
    disabled: false,
  };
  const mockSubmit = jest.fn();
  const label = 'label';
  const user = userEvent.setup();

  const getComponentWithFormik = withFormik(ATextarea, props);

  afterEach(cleanup);

  it('should render correctly', () => {
    render(getComponentWithFormik(mockSubmit));

    const componentATextarea = screen.getByTestId('ATextarea');

    expect(componentATextarea).toBeInTheDocument();
  });

  it('should not be focused with  "disabled=true" property', () => {
    render(getComponentWithFormik(mockSubmit, { label, disabled: true }));

    const textareatEl = screen.getByRole('textbox');
    const labelEl = screen.getByText(label);

    user.click(textareatEl);
    expect(textareatEl).not.toHaveFocus();

    expect(textareatEl).toHaveStyle({ borderColor: gray });
    expect(labelEl).toHaveStyle({ color: FONT_COLORS.gray });
  });

  it('should handle "label" property correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label }));

    const element = screen.getByText(label);

    expect(element.tagName).toBe('LABEL');
  });

  it('should handle "placeholder" property correctly', () => {
    const placeholder = 'placeholder';
    render(getComponentWithFormik(mockSubmit, { placeholder }));

    const element = screen.queryByPlaceholderText(placeholder);

    expect(element).toBeInTheDocument();
  });

  it('should handle "id" property correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label }));

    const inputEl = screen.getByRole('textbox');
    const labelEl = screen.getByText(label);

    expect(inputEl).toHaveAttribute('id', props.id);
    expect(labelEl).toHaveAttribute('for', props.id);
  });
});
