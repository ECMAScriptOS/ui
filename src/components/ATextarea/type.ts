export interface IATextarea {
  /**
   * Textarea id attribute
   */
  id: string;
  /**
   * Name of textarea field
   */
  name: string;
  /**
   * Label content.
   */
  label?: string;
  /**
   * Placeholder content.
   */
  placeholder?: string;
  /**
   * If true input cannot be focused.
   */
  disabled?: boolean;
}
