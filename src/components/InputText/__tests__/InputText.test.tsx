import { cleanup, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { withFormik } from '../../../utils/testDecorators';
import { COLORS, FONT_COLORS } from '../../../styles/colors';
import { InputText } from '../InputText';
import { IInputText } from '../type';

const { gray } = COLORS;

describe('__component: InputText', () => {
  const props: IInputText = {
    id: 'inputTextID',
    name: 'inputDateName',
    disabled: false,
  };
  const mockSubmit = jest.fn();
  const label = 'label';
  const user = userEvent.setup();

  const getComponentWithFormik = withFormik(InputText, props);

  afterEach(cleanup);

  it('should render correctly', () => {
    render(getComponentWithFormik(mockSubmit));

    const componentAInput = screen.getByTestId('InputText');

    expect(componentAInput).toBeInTheDocument();
  });

  it('should not be focused with  "disabled=true" property', () => {
    render(getComponentWithFormik(mockSubmit, { label, disabled: true }));

    const inputEl = screen.getByRole('textbox');
    const labelEl = screen.getByText(label);

    user.click(inputEl);
    expect(inputEl).not.toHaveFocus();

    expect(inputEl).toHaveStyle({ borderBottomColor: gray });
    expect(labelEl).toHaveStyle({ color: FONT_COLORS.gray });
  });

  it('should handle "label" property correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label }));

    const element = screen.getByText(label);

    expect(element.tagName).toBe('LABEL');
  });

  it('should handle "placeholder" property correctly', () => {
    const placeholder = 'placeholder';
    render(getComponentWithFormik(mockSubmit, { placeholder }));

    const element = screen.queryByPlaceholderText(placeholder);

    expect(element).toBeInTheDocument();
  });

  it('should handle "id" property correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label }));

    const inputEl = screen.getByRole('textbox');
    const labelEl = screen.getByText(label);

    expect(inputEl).toHaveAttribute('id', props.id);
    expect(labelEl).toHaveAttribute('for', props.id);
  });
});
