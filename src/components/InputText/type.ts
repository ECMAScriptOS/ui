export interface IInputText {
  /**
   * Input id attribute
   */
  id: string;
  /**
   * Name of input field
   */
  name: string;
  /**
   * input field type
   */
  type?: 'text' | 'email';
  /**
   * Label content.
   */
  label?: string;
  /**
   * Placeholder content.
   */
  placeholder?: string;
  /**
   * If true input cannot be focused.
   */
  disabled?: boolean;
}
