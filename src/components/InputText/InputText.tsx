import { FC, HTMLAttributes } from 'react';
import { Field, FieldProps } from 'formik';

import {
  StyledInput,
  StyledInputWrapper,
  StyledLabel,
  StyledValidationAlert,
  StyledWrapper,
} from './InputText.styles';
import { IInputText } from './type';

export const InputText: FC<IInputText & HTMLAttributes<HTMLInputElement>> = ({
  id,
  name,
  type = 'text',
  label,
  placeholder,
  disabled,
  className,
  ...rest
}) => (
  <Field name={name}>
    {({ field, meta }: FieldProps) => (
      <StyledWrapper data-testid="InputText" className={className}>
        {label && (
          <StyledLabel disabled={disabled} htmlFor={id}>
            {label}
          </StyledLabel>
        )}
        <StyledInputWrapper>
          <StyledInput
            id={id}
            type={type}
            placeholder={placeholder}
            isValid={!meta.touched || !meta.error}
            disabled={disabled}
            {...field}
            {...rest}
          />
        </StyledInputWrapper>
        {meta.touched && meta.error && (
          <StyledValidationAlert message={meta.error} />
        )}
      </StyledWrapper>
    )}
  </Field>
);
