import styled from 'styled-components';

import { COLORS, FONT_COLORS } from '../../styles/colors';
import { FlexColumnDirection } from '../../styles/common';
import { INPUT_PADDINGS } from '../../styles/metrics';
import { ValidationAlert } from '../../components/ValidationAlert/ValidationAlert';

const { primary, error, gray } = COLORS;
const { vertical, left, right } = INPUT_PADDINGS;

export const StyledWrapper = styled.div`
  position: relative;

  ${FlexColumnDirection};
`;

export const StyledLabel = styled.label<{ disabled?: boolean }>`
  margin-left: 1.2rem;
  color: ${({ disabled }) => disabled && FONT_COLORS.gray};
`;

export const StyledInputWrapper = styled.div`
  position: relative;
`;

export const StyledInput = styled.input<{
  isValid: boolean;
}>`
  border-bottom: 1px solid ${({ isValid }) => (isValid ? primary : error)};
  border-bottom-color: ${({ disabled }) => disabled && gray};
  padding: ${vertical} ${right} ${vertical} ${left};
  width: 100%;

  &:focus {
    box-shadow: 0 2px 0 0 ${({ isValid }) => (isValid ? primary : error)};
  }

  &::placeholder {
    color: ${({ disabled }) => disabled && 'transparent'};
  }
`;

export const StyledValidationAlert = styled(ValidationAlert)`
  position: absolute;
  top: 100%;
  width: 100%;
`;
