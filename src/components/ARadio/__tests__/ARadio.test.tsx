import { cleanup, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { FONT_COLORS } from '../../../styles/colors';
import { withFormik } from '../../../utils/testDecorators';
import { ARadio } from '../ARadio';
import { IARadio } from '../type';

describe('__component: ARadio', () => {
  const props: IARadio = {
    id: 'radioID',
    name: 'radioName',
    labelPosition: 'right',
    value: 'value',
  };
  const label = 'label';
  const mockSubmit = jest.fn();
  const user = userEvent.setup();

  const getComponentWithFormik = withFormik(ARadio, props);

  afterEach(cleanup);

  it('should render correctly', () => {
    render(getComponentWithFormik(mockSubmit));

    const component = screen.getByTestId('ARadio');

    expect(component).toBeInTheDocument();
  });

  it('should handle property "label" correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label }));

    const element = screen.getByLabelText(label);

    expect(element.tagName).toEqual('INPUT');
  });

  it('should handle property "id" correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label }));

    const inputEl = screen.getByRole('radio');
    const labelEl = screen.getByText(label);

    expect(labelEl).toHaveAttribute('for', props.id);
    expect(inputEl).toHaveAttribute('id', props.id);
  });

  it('should handle "labelPosition=right" property correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label }));

    const componentARadio = screen.getByTestId('ARadio');

    expect(componentARadio).toHaveStyle({ flexDirection: 'row-reverse' });
  });

  it('should handle "labelPosition=top" property correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label, labelPosition: 'top' }));

    const componentARadio = screen.getByTestId('ARadio');

    expect(componentARadio).toHaveStyle({ flexDirection: 'column' });
  });

  it('should handle "labelPosition=bottom" property correctly', () => {
    render(
      getComponentWithFormik(mockSubmit, { label, labelPosition: 'bottom' })
    );

    const componentARadio = screen.getByTestId('ARadio');

    expect(componentARadio).toHaveStyle({ flexDirection: 'column-reverse' });
  });

  it('should handle "labelPosition=left" property correctly', () => {
    render(
      getComponentWithFormik(mockSubmit, { label, labelPosition: 'left' })
    );

    const componentARadio = screen.getByTestId('ARadio');

    expect(componentARadio).toHaveStyle({ flexDirection: 'row' });
  });

  it('should handle "disabled=true" property correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label, disabled: true }));

    const labelEl = screen.getByText(label);
    const inputEl = screen.getByRole('radio');

    user.click(inputEl);

    expect(inputEl).toBeDisabled();
    expect(inputEl).not.toBeChecked();
    expect(labelEl).toHaveStyle({ color: FONT_COLORS.gray });
  });
});
