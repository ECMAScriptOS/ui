import styled from 'styled-components';

import { COLORS, FONT_COLORS } from '../../styles/colors';
import { FlexCenteredXY, getFlexDirection } from '../../styles/common';
import { LabelPositionType } from '../../types';

const { primary, white, gray } = COLORS;

export const StyledWrapper = styled.div<{ labelPosition?: LabelPositionType }>`
  ${FlexCenteredXY};
  flex-direction: ${({ labelPosition }) => getFlexDirection(labelPosition)};
  gap: 8px;
`;

export const StyledLabel = styled.label<{ disabled?: boolean }>`
  color: ${({ disabled }) =>
    disabled ? FONT_COLORS.gray : FONT_COLORS.primary};
  cursor: ${({ disabled }) => !disabled && 'pointer'};
`;

export const StyledRadioInputWrapper = styled.div`
  position: relative;
  width: 16px;
  height: 16px;
`;

export const StyledRadioInput = styled.input`
  position: absolute;
  top: 0;
  left: 0;
  width: inherit;
  height: inherit;
  z-index: 1;
  opacity: 0;

  &:checked ~ div:after {
    opacity: 1;
  }

  &:not(:checked):not(:disabled):hover ~ div:after {
    opacity: 0.6;
  }

  &:disabled ~ div {
    box-shadow: 0 0 0 1px ${gray};
  }
`;

export const StyledRadio = styled.div`
  ${FlexCenteredXY};
  position: absolute;
  top: 0;
  left: 0;
  box-shadow: 0 0 0 1px ${primary};
  border-radius: 50%;
  width: inherit;
  height: inherit;

  &:after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0;
    width: inherit;
    height: inherit;
    border: 3px solid ${white};
    border-radius: 50%;
    background-color: ${primary};
  }
`;
