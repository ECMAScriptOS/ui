import { LabelPositionType } from '../../types';

export interface IARadio {
  /**
   * Radio input id
   */
  id: string;
  /**
   * Visible label
   */
  label?: string;
  /**
   * Location for radio label
   */
  labelPosition?: LabelPositionType;
  /**
   * When it's true label will be disabled
   */
  disabled?: boolean;
  /**
   * Name of radio input
   */
  name: string;
  /**
   * Radio input value
   */
  value: string | number;
}
