import { FC } from 'react';
import { Field, FieldProps } from 'formik';

import {
  StyledLabel,
  StyledRadio,
  StyledRadioInput,
  StyledRadioInputWrapper,
  StyledWrapper,
} from './ARadio.styles';
import { IARadio } from './type';

export const ARadio: FC<IARadio> = ({
  name,
  id,
  label,
  labelPosition,
  disabled,
  value,
}) => (
  <Field type="radio" name={name} value={value}>
    {({ field }: FieldProps) => (
      <StyledWrapper data-testid="ARadio" labelPosition={labelPosition}>
        {label && (
          <StyledLabel disabled={disabled} htmlFor={id}>
            {label}
          </StyledLabel>
        )}
        <StyledRadioInputWrapper>
          <StyledRadioInput
            type="radio"
            id={id}
            disabled={disabled}
            {...field}
          />
          <StyledRadio />
        </StyledRadioInputWrapper>
      </StyledWrapper>
    )}
  </Field>
);
