export interface IUserBadgeExtraMinimized {
  userPageLink: string;
  photoURL: string;
  isOnline: boolean;
}

export interface IUserBadgeMinimized extends IUserBadgeExtraMinimized {
  firstName: string;
  lastName: string;
}

export interface IUserBadgeNormal extends IUserBadgeMinimized {
  age: string;
  nationality: string;
}

export type IUserBadge =
  | ({ type: 'extraMinimized' } & IUserBadgeExtraMinimized)
  | ({ type: 'minimized' } & IUserBadgeMinimized)
  | ({ type: 'normal' } & IUserBadgeNormal);
