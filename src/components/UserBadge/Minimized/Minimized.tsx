import { FC } from 'react';

import { IUserBadgeMinimized } from '../type';
import { StyledName, StyledPhoto, StyledWrapper } from './Minimized.styles';

export const Minimized: FC<IUserBadgeMinimized> = ({
  userPageLink,
  firstName,
  lastName,
  photoURL,
  isOnline,
}) => (
  <StyledWrapper
    to={userPageLink}
    isOnline={isOnline}
    dataTestId="UserBadgeMinimized"
  >
    <StyledPhoto src={photoURL} alt="user photo" isOnline={isOnline} />
    <StyledName>
      {firstName} {lastName}
    </StyledName>
  </StyledWrapper>
);
