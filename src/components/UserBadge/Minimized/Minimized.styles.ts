import styled from 'styled-components';

import { ALink } from '../../ALink';
import { COLORS } from '../../../styles/colors';
import { BORDER_RADIUS } from '../../../styles/metrics';

export const StyledWrapper = styled(ALink)<{
  isOnline: boolean;
}>`
  position: relative;
  display: flex;
  width: fit-content;
  column-gap: 7px;
  align-items: center;
  text-transform: none;
  border-radius: ${BORDER_RADIUS.normal};
  max-width: 170px;

  ${({ isOnline }) =>
    `
        border: 1px solid ${isOnline ? COLORS.success : COLORS.gray};
    `}

  padding: 1px 5px 1px 1px;
  transition: background-color 0.2s;

  &:hover {
    text-decoration: none;
    background-color: ${COLORS.gray};
    transition: background-color 0.2s;
  }
`;

export const StyledPhoto = styled.img<{
  isOnline: boolean;
}>`
  width: 30px;
  aspect-ratio: 1/1;
  object-fit: cover;
  border-radius: 50%;

  border: ${({ isOnline }) => isOnline && '2px solid ' + COLORS.success};
`;
export const StyledName = styled.p`
  text-overflow: ellipsis;
  overflow: hidden;
  font-size: 0.75rem;
`;
