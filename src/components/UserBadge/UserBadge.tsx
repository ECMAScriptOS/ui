import { FC } from 'react';

import { IUserBadge } from './type';
import { Minimized } from './Minimized/Minimized';
import { Normal } from './Normal/Normal';
import { ExtraMinimized } from './ExtraMinimized/ExtraMinimized';

export const UserBadge: FC<IUserBadge> = (props) => {
  switch (props.type) {
    case 'minimized':
      return <Minimized {...props} />;
    case 'extraMinimized':
      return <ExtraMinimized {...props} />;
    default:
      return <Normal {...props} />;
  }
};
