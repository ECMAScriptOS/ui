import styled from 'styled-components';

import { ALink } from '../../ALink';
import { COLORS } from '../../../styles/colors';
import { PADDINGS } from '../../../styles/metrics';
import { FlexColumnDirection } from '../../../styles/common';

export const StyledWrapper = styled(ALink)<{ isOnline: boolean }>`
  position: relative;
  width: 210px;
  height: 80px;
  text-transform: none;
  overflow: hidden;

  &:hover > div:first-child > img {
    transform: translate(-53%, -50%) scale(0.9);
    transition: transform 0.8s;
  }

  &:hover > div:last-child p,
  &:hover > div:last-child h4 {
    backdrop-filter: blur(1px);
    transition: backdrop-filter 0.5s;
  }

  & > div:first-child > img {
    transition: transform 0.8s;
  }

  & > div:last-child p,
  & > div:last-child h4 {
    transition: backdrop-filter 0.5s;
  }

  &::after {
    content: '';
    position: absolute;
    top: 3px;
    left: 3px;
    height: 10px;
    width: 10px;
    border-radius: 50%;
    background-color: ${({ isOnline }) =>
      isOnline ? COLORS.success : COLORS.gray};
  }
`;
export const StyledPhotoWrapper = styled.div`
  position: relative;
  width: inherit;
  height: inherit;
`;

export const StyledPhoto = styled.img`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 120%;
`;

export const StyledUserInfo = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  text-align: right;
  font-size: 0.75rem;
  height: inherit;
  max-width: 70%;
  padding: ${PADDINGS.vertical} ${PADDINGS.horizontal};
  ${FlexColumnDirection};

  & p,
  & h4 {
    mix-blend-mode: difference;
    backdrop-filter: blur(21px);
    color: ${COLORS.gray};
  }

  & p {
    align-self: end;
    width: fit-content;
  }
`;

export const StyledName = styled.h4`
  white-space: nowrap;
  text-overflow: ellipsis;
  font-size: 1rem;
  overflow: hidden;
  margin-bottom: 7px;
`;
