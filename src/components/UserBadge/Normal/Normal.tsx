import { FC } from 'react';

import { IUserBadgeNormal } from '../type';
import {
  StyledPhoto,
  StyledPhotoWrapper,
  StyledWrapper,
  StyledUserInfo,
  StyledName,
} from './Normal.styles';

export const Normal: FC<IUserBadgeNormal> = ({
  userPageLink,
  firstName,
  lastName,
  photoURL,
  age,
  nationality,
  isOnline,
}) => (
  <StyledWrapper
    to={userPageLink}
    isOnline={isOnline}
    dataTestId="UserBadgeNormal"
  >
    <StyledPhotoWrapper>
      <StyledPhoto src={photoURL} alt="user photo" />
    </StyledPhotoWrapper>
    <StyledUserInfo>
      <StyledName title={firstName}>
        {firstName} {lastName}
      </StyledName>
      <p>{nationality}</p>
      <p>{age}</p>
    </StyledUserInfo>
  </StyledWrapper>
);
