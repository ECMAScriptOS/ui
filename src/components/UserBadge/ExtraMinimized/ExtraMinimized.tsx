import { FC } from 'react';

import { IUserBadgeExtraMinimized } from '../type';
import { StyledPhoto } from './ExtraMinimized.styles';
import { ALink } from '../../ALink';

export const ExtraMinimized: FC<IUserBadgeExtraMinimized> = ({
  userPageLink,
  photoURL,
  isOnline,
}) => (
  <ALink to={userPageLink} dataTestId="UserBadgeExtraMinimized">
    <StyledPhoto src={photoURL} alt="user photo" isOnline={isOnline} />
  </ALink>
);
