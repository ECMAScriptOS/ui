import styled from 'styled-components';

import { COLORS } from '../../../styles/colors';

export const StyledPhoto = styled.img<{
  isOnline: boolean;
}>`
  width: 25px;
  height: 25px;
  aspect-ratio: 1/1;
  object-fit: cover;
  border-radius: 50%;

  ${({ isOnline }) =>
    !isOnline &&
    `
      -webkit-filter: grayscale(100%); 
      filter: grayscale(100%);
    `}

  border: ${({ isOnline }) => isOnline && '2px solid ' + COLORS.success};
`;
