import { cleanup, screen } from '@testing-library/react';

import { IUserBadge } from '../type';
import { UserBadge } from '../UserBadge';
import { renderWithRouter } from '../../../utils/testDecorators';
import { COLORS } from '../../../styles/colors';

describe('__component: UserBadgeNormal', () => {
  const props: IUserBadge = {
    type: 'normal',
    userPageLink: '/userLink',
    firstName: 'Name',
    lastName: 'Surname',
    age: '41',
    nationality: 'USA',
    photoURL: 'url.test',
    isOnline: false,
  };

  afterEach(cleanup);

  it('should render correctly', () => {
    renderWithRouter(<UserBadge {...props} />);

    const componentUserBadgeNormal = screen.getByTestId('UserBadgeNormal');

    expect(componentUserBadgeNormal).toBeInTheDocument();
    expect(componentUserBadgeNormal).toHaveAttribute(
      'href',
      props.userPageLink
    );
  });

  it('should handle properties "firstName", "lastName" correctly', () => {
    renderWithRouter(<UserBadge {...props} />);

    const userNameEl = screen.getByRole('heading', { level: 4 });

    expect(userNameEl).toBeInTheDocument();
    expect(userNameEl).toHaveTextContent(
      props.firstName + ' ' + props.lastName
    );
  });

  it('should handle property "age" correctly', () => {
    renderWithRouter(<UserBadge {...props} />);

    const userAgeEl = screen.getByText(props.age);

    expect(userAgeEl).toBeInTheDocument();
  });

  it('should handle property "nationality" correctly', () => {
    renderWithRouter(<UserBadge {...props} />);

    const userNationalityEl = screen.getByText(props.nationality);

    expect(userNationalityEl).toBeInTheDocument();
  });

  it('should handle property "photoURL" correctly', () => {
    renderWithRouter(<UserBadge {...props} />);

    const userPhotoEl = screen.getByRole('img');

    expect(userPhotoEl).toBeInTheDocument();
    expect(userPhotoEl).toHaveAttribute('src', props.photoURL);
  });
});

describe('__component: UserBadgeMinimized', () => {
  const props: IUserBadge = {
    type: 'minimized',
    userPageLink: '/userLink',
    firstName: 'Name',
    lastName: 'Surname',
    age: '41',
    nationality: 'USA',
    photoURL: 'url.test',
    isOnline: false,
  };

  afterEach(cleanup);

  it('should render correctly', () => {
    renderWithRouter(<UserBadge {...props} />);

    const componentUserBadgeMinimized =
      screen.getByTestId('UserBadgeMinimized');

    expect(componentUserBadgeMinimized).toBeInTheDocument();
    expect(componentUserBadgeMinimized).toHaveAttribute(
      'href',
      props.userPageLink
    );
  });

  it('should handle properties "firstName", "lastName" correctly', () => {
    renderWithRouter(<UserBadge {...props} />);

    const userNameEl = screen.getByText(props.firstName + ' ' + props.lastName);

    expect(userNameEl).toBeInTheDocument();
  });

  it('should handle property "photoURL" correctly', () => {
    renderWithRouter(<UserBadge {...props} />);

    const userPhotoEl = screen.getByRole('img');

    expect(userPhotoEl).toBeInTheDocument();
    expect(userPhotoEl).toHaveAttribute('src', props.photoURL);
  });

  it('should handle property "isOnline=false" correctly', () => {
    renderWithRouter(<UserBadge {...props} />);

    const componentUserBudgeMinimized = screen.getByRole('link');

    expect(componentUserBudgeMinimized).toBeInTheDocument();
    expect(componentUserBudgeMinimized).toHaveStyle({
      border: '1px solid ' + COLORS.gray,
    });
  });

  it('should handle property "isOnline=true" correctly', () => {
    renderWithRouter(<UserBadge {...props} isOnline />);

    const componentUserBudgeMinimized = screen.getByRole('link');
    const userPhotoEl = screen.getByRole('img');

    expect(componentUserBudgeMinimized).toBeInTheDocument();
    expect(componentUserBudgeMinimized).toHaveStyle({
      border: '1px solid ' + COLORS.success,
    });
    expect(userPhotoEl).toBeInTheDocument();
    expect(userPhotoEl).toHaveStyle({
      border: '2px solid ' + COLORS.success,
    });
  });
});

describe('__component: UserBadgeExtraMinimized', () => {
  const props: IUserBadge = {
    type: 'extraMinimized',
    userPageLink: '/userLink',
    firstName: 'Name',
    lastName: 'Surname',
    age: '41',
    nationality: 'USA',
    photoURL: 'url.test',
    isOnline: false,
  };

  afterEach(cleanup);

  it('should render correctly', () => {
    renderWithRouter(<UserBadge {...props} />);

    const componentUserBadgeExtraMinimized = screen.getByTestId(
      'UserBadgeExtraMinimized'
    );

    expect(componentUserBadgeExtraMinimized).toBeInTheDocument();
    expect(componentUserBadgeExtraMinimized).toHaveAttribute(
      'href',
      props.userPageLink
    );
  });

  it('should handle property "photoURL" correctly', () => {
    renderWithRouter(<UserBadge {...props} />);

    const userPhotoEl = screen.getByRole('img');

    expect(userPhotoEl).toBeInTheDocument();
    expect(userPhotoEl).toHaveAttribute('src', props.photoURL);
  });

  it('should handle property "isOnline=false" correctly', () => {
    renderWithRouter(<UserBadge {...props} />);

    const userPhotoEl = screen.getByRole('img');

    expect(userPhotoEl).toBeInTheDocument();
    expect(userPhotoEl).toHaveStyle({
      filter: 'grayscale(100%)',
    });
  });

  it('should handle property "isOnline=true" correctly', () => {
    renderWithRouter(<UserBadge {...props} isOnline />);

    const userPhotoEl = screen.getByRole('img');

    expect(userPhotoEl).toBeInTheDocument();
    expect(userPhotoEl).toHaveStyle({
      border: '2px solid ' + COLORS.success,
    });
  });
});
