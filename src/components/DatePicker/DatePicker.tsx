import { FC, useRef } from 'react';
import { Form, FormikProvider, useFormik } from 'formik';
import { ReactComponent as CompletedIcon } from 'assets/icons/common/checked.svg';

import { useClickOutsideDetector } from '../../hooks/useClickOutsideDetector';
import { parseDate } from '../../utils/parseDate';
import { InputNumber } from '../InputNumber/InputNumber';
import { ASelect } from '../ASelect/ASelect';
import { AButton } from '../AButton/AButton';
import { IDatePicker } from './type';
import { StyledWrapper } from './DatePicker.styles';

export const DatePicker: FC<IDatePicker> = ({
  setIsVisible,
  handleSubmit,
  coordinates,
  defaultValue,
  months,
  label,
}) => {
  const elementRef = useRef<HTMLDivElement>(null);

  useClickOutsideDetector(elementRef, setIsVisible);

  const monthOptions = months.map((month, index) => ({
    label: month,
    value: index,
  }));

  const parsedDefaultValue = parseDate(defaultValue);

  const formik = useFormik({
    initialValues: {
      day: parsedDefaultValue.day,
      month: parsedDefaultValue.month,
      year: parsedDefaultValue.year,
    },
    onSubmit: ({ day, month, year }) => {
      handleSubmit(day, month, year);

      setIsVisible(false);
    },
  });

  return (
    <StyledWrapper
      data-testid="DatePicker"
      ref={elementRef}
      coordinates={coordinates}
    >
      <FormikProvider value={formik}>
        <Form>
          <InputNumber name="day" id="dayID" placeholder={label.day} />
          <ASelect
            name="month"
            id="monthID"
            options={monthOptions}
            placeholder={label.month}
          />
          <InputNumber name="year" id="yearID" placeholder={label.year} />
          <AButton type="submit" icon={<CompletedIcon title="confirm" />} />
        </Form>
      </FormikProvider>
    </StyledWrapper>
  );
};
