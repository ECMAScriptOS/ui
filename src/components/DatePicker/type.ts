export interface IDatePicker {
  /**
   * Handle DatePicker visibility
   * @param isVisible
   */
  setIsVisible: (isVisible: boolean) => void;
  /**
   * Submit event
   */
  handleSubmit: (day: number, month: number, year: number) => void;
  /**
   * Coordinates for detecting element position
   */
  coordinates: { x: number; y: number };
  /**
   * Providing default value
   */
  defaultValue: string | number;
  /**
   * Months
   */
  months: string[];
  /**
   * Label object
   */
  label: { day: string; month: string; year: string };
}
