import styled from 'styled-components';

import { COLORS } from '../../styles/colors';
import { PADDINGS } from '../../styles/metrics';

const { vertical, horizontal } = PADDINGS;
const { primary, gray, white } = COLORS;

export const StyledWrapper = styled.div<{
  coordinates: { x: number; y: number };
}>`
  position: absolute;
  top: ${({ coordinates }) => coordinates.y}px;
  left: ${({ coordinates }) => coordinates.x}px;
  box-shadow: 3px 2px 0 ${gray};
  max-width: 330px;
  transform: translateY(-100%);
  padding: ${vertical} ${horizontal};
  background-color: ${white};
  border: 1px solid ${primary};

  & form {
    display: flex;
    align-items: center;
    column-gap: 4px;
  }

  & form > div:first-child input {
    width: 60px;
  }

  & form > div:nth-child(2) input {
    width: 120px;
  }

  & form > div:last-child {
    flex: 1;
  }
`;
