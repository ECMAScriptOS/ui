import { cleanup, render, screen } from '@testing-library/react';

import { DatePicker } from '../DatePicker';
import { IDatePicker } from '../type';
import { labelDatePicker, months } from '../datePicker.mock';

describe('__component: DatePicker', () => {
  const props: IDatePicker = {
    defaultValue: '2000-02-01',
    coordinates: { x: 0, y: 0 },
    handleSubmit: jest.fn(),
    setIsVisible: jest.fn(),
    months: months,
    label: labelDatePicker,
  };

  afterEach(cleanup);

  it('should render correctly', () => {
    render(<DatePicker {...props} />);

    const component = screen.getByTestId('DatePicker');

    expect(component).toBeInTheDocument();
  });
});
