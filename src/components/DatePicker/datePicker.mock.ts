export const months = [
  'january',
  'february',
  'march',
  'april',
  'may',
  'june',
  'july',
  'august',
  'september',
  'october',
  'november',
  'december',
];

export const labelDatePicker = {
  day: 'day',
  month: 'month',
  year: 'year',
};
