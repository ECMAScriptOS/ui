import styled from 'styled-components';

import { COLORS } from '../../styles/colors';
import { FlexCenteredXY, FlexColumnDirection } from '../../styles/common';
import { INPUT_PADDINGS } from '../../styles/metrics';
import { ValidationAlert } from '../../components/ValidationAlert/ValidationAlert';

const { primary, error } = COLORS;
const { vertical, left, right } = INPUT_PADDINGS;
export const StyledWrapper = styled.div`
  position: relative;

  ${FlexColumnDirection};
`;

export const StyledLabel = styled.label`
  margin-left: 1.2rem;
`;

export const StyledInputWrapper = styled.div`
  position: relative;

  & :not(input:focus) ~ div > div:first-child button svg,
  & :not(input:focus) ~ div > div:last-child button svg {
    fill: transparent;
  }
`;

export const StyledInput = styled.input<{ isValid: boolean }>`
  border-bottom: 1px solid ${({ isValid }) => (isValid ? primary : error)};
  width: 100%;
  padding: ${vertical} ${right} ${vertical} ${left};

  &::-webkit-outer-spin-button,
  &::-webkit-inner-spin-button {
    display: none;
  }

  &:focus {
    box-shadow: 0 2px 0 0 ${({ isValid }) => (isValid ? primary : error)};
  }
`;

export const StyledActionButtons = styled.div`
  ${FlexCenteredXY};
  flex-direction: column;
  position: absolute;
  top: 0;
  right: 0;
  height: 100%;

  & button {
    height: 100%;
    padding: 0 8px;
  }
`;

export const StyledValidationAlert = styled(ValidationAlert)`
  position: absolute;
  top: 100%;
  width: 100%;
`;
