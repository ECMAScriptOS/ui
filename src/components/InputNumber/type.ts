export interface IInputNumber {
  /**
   * Input id attribute
   */
  id: string;
  /**
   * Name of input field
   */
  name: string;
  /**
   * Visible label
   */
  label?: string;
  /**
   * Minimum value of field
   */
  min?: number;
  /**
   * Maximum value of field
   */
  max?: number;
  /**
   * Placeholder of input number
   */
  placeholder?: string;
}
