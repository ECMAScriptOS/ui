import { cleanup, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { withFormik } from '../../../utils/testDecorators';
import { InputNumber } from '../InputNumber';
import { IInputNumber } from '../type';

describe('__component: InputNumber', () => {
  const props: IInputNumber = {
    id: 'inputNumberID',
    name: 'inputNumberName',
  };
  const label = 'Label';
  const mockSubmit = jest.fn();
  const user = userEvent.setup();

  const getComponentWithFormik = withFormik(InputNumber, props);

  afterEach(cleanup);

  it('should render correctly', () => {
    render(getComponentWithFormik(mockSubmit));

    const component = screen.getByTestId('InputNumber');

    expect(component).toBeInTheDocument();
  });

  it('should handle "id" property correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label }));

    const inputEl = screen.getByRole('spinbutton');
    const labelEl = screen.getByText(label);

    expect(inputEl).toHaveAttribute('id', props.id);
    expect(labelEl).toHaveAttribute('for', props.id);
  });

  it('should handle "label" property correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label }));

    const labelEl = screen.getByText(label);

    expect(labelEl.tagName).toEqual('LABEL');
  });

  it('should handle "min" property correctly. Invalid case.', async () => {
    const min = 3;
    render(getComponentWithFormik(mockSubmit, { min }));

    const inputEl = screen.getByRole('spinbutton');

    await user.type(inputEl, '2');

    expect(inputEl).toBeInvalid();
  });

  it('should handle "min" property correctly. Valid case.', async () => {
    const min = 3;
    render(getComponentWithFormik(mockSubmit, { min }));

    const inputEl = screen.getByRole('spinbutton');

    await user.type(inputEl, '3');

    expect(inputEl).toBeValid();
  });

  it('should handle "max" property correctly. Invalid case.', async () => {
    const max = 10;
    render(getComponentWithFormik(mockSubmit, { max }));

    const inputEl = screen.getByRole('spinbutton');

    await user.type(inputEl, '11');

    expect(inputEl).toBeInvalid();
  });

  it('should handle "max" property correctly. Valid case.', async () => {
    const max = 10;
    render(getComponentWithFormik(mockSubmit, { max }));

    const inputEl = screen.getByRole('spinbutton');

    await user.type(inputEl, '3');

    expect(inputEl).toBeValid();
  });

  it('should handle "placeholder" property correctly', () => {
    const placeholder = 'placeholder';
    render(getComponentWithFormik(mockSubmit, { placeholder }));

    const element = screen.queryByPlaceholderText(placeholder);

    expect(element).toBeInTheDocument();
  });

  it('should handle click on increase button correctly', async () => {
    const placeholder = 'placeholder';
    const min = 5;
    render(getComponentWithFormik(mockSubmit, { placeholder, min }));

    const inputEl = screen.getByRole('spinbutton');
    const increaseButtonEl = screen.getAllByRole('button')[0];

    await user.click(increaseButtonEl);

    expect(inputEl).toHaveValue(min);

    await user.click(increaseButtonEl);

    await user.click(increaseButtonEl);

    expect(inputEl).toHaveValue(min + 2);
  });

  it('should handle click on decrease button correctly', async () => {
    const placeholder = 'placeholder';
    const max = 130;
    render(getComponentWithFormik(mockSubmit, { placeholder, max }));

    const inputEl = screen.getByRole('spinbutton');
    const decreaseButtonEl = screen.getAllByRole('button')[1];

    await user.click(decreaseButtonEl);

    expect(inputEl).toHaveValue(max);

    await user.click(decreaseButtonEl);

    await user.click(decreaseButtonEl);

    expect(inputEl).toHaveValue(max - 2);
  });
});
