import { FC, HTMLAttributes } from 'react';
import { Field, FieldProps } from 'formik';
import { ReactComponent as IncreaseIcon } from 'assets/icons/common/plus.svg';
import { ReactComponent as DecreaseIcon } from 'assets/icons/common/minus.svg';

import { AButton } from '../AButton/AButton';
import { IInputNumber } from './type';
import {
  StyledActionButtons,
  StyledInput,
  StyledValidationAlert,
  StyledInputWrapper,
  StyledLabel,
  StyledWrapper,
} from './InputNumber.styles';

export const InputNumber: FC<
  IInputNumber & HTMLAttributes<HTMLInputElement>
> = ({ id, name, label, placeholder, min, max, className, ...rest }) => {
  const getIncreasedInputValue = (currentValue?: number) => {
    if (max && currentValue === max) return currentValue;

    return currentValue ? currentValue + 1 : min || 1;
  };

  const getDecreasedInputValue = (currentValue?: number) => {
    if (min && currentValue === min) return currentValue;

    return currentValue ? currentValue - 1 : max || 1;
  };

  return (
    <Field name={name}>
      {({ field, meta, form }: FieldProps) => (
        <StyledWrapper data-testid="InputNumber" className={className}>
          {label && <StyledLabel htmlFor={id}>{label}</StyledLabel>}
          <StyledInputWrapper>
            <StyledInput
              id={id}
              type="number"
              isValid={!meta.error}
              min={min}
              max={max}
              placeholder={placeholder}
              {...field}
              {...rest}
            />
            <StyledActionButtons>
              <AButton
                icon={<IncreaseIcon title="increase" />}
                handleClick={() =>
                  form.setFieldValue(name, getIncreasedInputValue(field.value))
                }
              />
              <AButton
                icon={<DecreaseIcon title="decrease" />}
                handleClick={() =>
                  form.setFieldValue(name, getDecreasedInputValue(field.value))
                }
              />
            </StyledActionButtons>
          </StyledInputWrapper>
          {meta.error && <StyledValidationAlert message={meta.error} />}
        </StyledWrapper>
      )}
    </Field>
  );
};
