import styled from 'styled-components';

import { COLORS } from '../../styles/colors';
import { BORDER_RADIUS, PADDINGS } from '../../styles/metrics';

export const StyledWrapper = styled.div<{ isAuthor: boolean; isRead: boolean }>`
  position: relative;
  border: 1px solid ${COLORS.gray};
  padding: ${PADDINGS.vertical} ${PADDINGS.horizontal};
  border-radius: ${BORDER_RADIUS.normal};
  background-color: ${({ isRead }) => (isRead ? 'transparent' : COLORS.gray)};
  width: fit-content;
    
  ${({ isAuthor }) =>
    isAuthor
      ? 'border-bottom-right-radius: 0;'
      : 'border-bottom-left-radius: 0;'};    
}



`;

export const StyledMessage = styled.p<{ isContentExpanded: boolean }>`
  ${({ isContentExpanded }) =>
    !isContentExpanded &&
    `
      display: -webkit-box;
      overflow: hidden;
      -webkit-line-clamp: 3;
      -webkit-box-orient: vertical;
      text-overflow: ellipsis;
    `}

  overflow-wrap: break-word;
`;

export const StyledButton = styled.button`
  position: absolute;
  right: 0;
  bottom: 0;
  font-size: 0.75rem;
  text-transform: lowercase;
  opacity: 0.5;

  &:hover {
    opacity: 1;
  }
`;
