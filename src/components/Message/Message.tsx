import { FC, useEffect, useRef, useState } from 'react';

import { StyledButton, StyledMessage, StyledWrapper } from './Message.styles';
import { IMessage } from './type';

export const Message: FC<IMessage> = ({
  content,
  isAuthor,
  isRead,
  expandButtonContent,
  shrinkButtonContent,
  className,
}) => {
  const [isContentExpanded, setIsContentExpanded] = useState(false);
  const [isButtonVisible, setIsButtonVisible] = useState(false);

  const paragraphRef = useRef<HTMLParagraphElement>(null);

  useEffect(() => {
    if (!paragraphRef.current) return;

    const fullHeightParagraphDOMElement = paragraphRef.current.scrollHeight;
    const displayedHeightParagraphDOMElement =
      paragraphRef.current.offsetHeight;

    if (fullHeightParagraphDOMElement > displayedHeightParagraphDOMElement) {
      setIsButtonVisible(true);
    }
  }, []);

  const handleClickIsContentExpandedToggle = () => {
    setIsContentExpanded(!isContentExpanded);
  };

  return (
    <StyledWrapper
      isAuthor={isAuthor}
      isRead={isRead}
      data-testid="Message"
      className={className}
    >
      <StyledMessage ref={paragraphRef} isContentExpanded={isContentExpanded}>
        {content}
      </StyledMessage>
      {isButtonVisible && (
        <StyledButton onClick={handleClickIsContentExpandedToggle}>
          {isContentExpanded ? shrinkButtonContent : expandButtonContent}...
        </StyledButton>
      )}
    </StyledWrapper>
  );
};
