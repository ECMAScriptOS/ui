import { cleanup, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { COLORS } from '../../../styles/colors';
import { IMessage } from '../type';
import { Message } from '../Message';

describe('__component: Message', () => {
  const user = userEvent.setup();
  const props: IMessage = {
    content: 'Some content.',
    isRead: true,
    isAuthor: true,
    shrinkButtonContent: 'Less',
    expandButtonContent: 'More',
  };

  afterEach(cleanup);

  it('should render correctly', () => {
    render(<Message {...props} />);

    const componentMessage = screen.getByTestId('Message');

    expect(componentMessage).toBeInTheDocument();
  });

  it('should handle property "isAuthor=true" correctly', () => {
    render(<Message {...props} />);

    const componentMessage = screen.getByTestId('Message');

    expect(componentMessage).toHaveStyle({
      borderBottomRightRadius: 0,
    });
  });

  it('should handle property "isAuthor=false" correctly', () => {
    render(<Message {...props} isAuthor={false} />);

    const componentMessage = screen.getByTestId('Message');

    expect(componentMessage).toHaveStyle({
      borderBottomLeftRadius: 0,
    });
  });

  it('should handle property "isRead=true" correctly', () => {
    render(<Message {...props} />);

    const componentMessage = screen.getByTestId('Message');

    expect(componentMessage).toHaveStyle({
      background: 'transparent',
    });
  });

  it('should handle property "isRead=false" correctly', () => {
    render(<Message {...props} isRead={false} />);

    const componentMessage = screen.getByTestId('Message');

    expect(componentMessage).toHaveStyle({
      background: COLORS.gray,
    });
  });

  it('should handle property "content" correctly', () => {
    render(<Message {...props} />);

    const contentEl = screen.getByText(props.content);

    expect(contentEl).toBeInTheDocument();
    expect(contentEl.tagName).toBe('P');
  });

  it('should handle property "expandButtonContent" correctly', async () => {
    Object.defineProperty(HTMLElement.prototype, 'scrollHeight', {
      configurable: true,
      value: 200,
    });

    render(<Message {...props} />);

    const buttonEl = screen.getByRole('button');

    expect(buttonEl.textContent).toBe(props.expandButtonContent + '...');

    await user.click(buttonEl);
    await user.click(buttonEl);

    expect(buttonEl.textContent).toBe(props.expandButtonContent + '...');
  });

  it('should handle property "shrinkButtonContent" correctly', async () => {
    Object.defineProperty(HTMLElement.prototype, 'scrollHeight', {
      configurable: true,
      value: 200,
    });

    render(<Message {...props} />);

    const buttonEl = screen.getByRole('button');

    await user.click(buttonEl);

    expect(buttonEl.textContent).toBe(props.shrinkButtonContent + '...');
  });

  it('should not render button if full text is displayed', async () => {
    Object.defineProperty(HTMLElement.prototype, 'scrollHeight', {
      configurable: true,
      value: 200,
    });

    Object.defineProperty(HTMLElement.prototype, 'offsetHeight', {
      configurable: true,
      value: 300,
    });

    render(<Message {...props} />);

    const buttonEl = screen.queryByRole('button');

    expect(buttonEl).not.toBeInTheDocument();
  });
});
