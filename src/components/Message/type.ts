export interface IMessage {
  /**
   * Message content
   */
  content: string;
  /**
   * Message is received or sent
   */
  isAuthor: boolean;
  /**
   * Whether message has been read or not
   */
  isRead: boolean;
  /**
   * Shrink button content
   */
  shrinkButtonContent: string;
  /**
   * Expand button content
   */
  expandButtonContent: string;
  /**
   * For customising component
   */
  className?: string;
}
