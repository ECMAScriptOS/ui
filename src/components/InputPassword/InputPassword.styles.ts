import styled from 'styled-components';

import { COLORS } from '../../styles/colors';
import { FlexColumnDirection } from '../../styles/common';
import { INPUT_PADDINGS } from '../../styles/metrics';
import { ValidationAlert } from '../../components/ValidationAlert/ValidationAlert';
import { AButton } from '../AButton/AButton';
import { ValidationAlertType } from '../ValidationAlert/type';

const { vertical, left, right } = INPUT_PADDINGS;

export const StyledWrapper = styled.div`
  position: relative;

  ${FlexColumnDirection};
`;

export const StyledLabel = styled.label`
  margin-left: 1.2rem;
`;

// ToDo: Improve password mask
export const StyledInputWrapper = styled.div<{
  validationType?: ValidationAlertType;
}>`
  position: relative;

  & input {
    border-bottom-color: ${({ validationType }) =>
      COLORS[validationType || 'primary']};

    &::placeholder {
      font-size: 1rem;
    }

    &:focus {
      box-shadow: 0 2px 0 0
        ${({ validationType }) => {
          return COLORS[validationType || 'primary'];
        }};
    }
  }
`;

export const StyledInput = styled.input`
  border-bottom: 1px solid ${COLORS.primary};
  width: 100%;
  padding: ${vertical} ${right} ${vertical} ${left};
`;

export const StyledToggleVisibilityButton = styled(AButton)<{
  passwordIsVisible: boolean;
}>`
  position: absolute;
  top: 0;
  right: 4px;
  height: 100%;
`;

export const StyledValidationAlert = styled(ValidationAlert)`
  position: absolute;
  top: 100%;
  width: 100%;
`;
