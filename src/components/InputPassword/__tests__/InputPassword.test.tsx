import { cleanup, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { withFormik } from '../../../utils/testDecorators';
import { InputPassword } from '../InputPassword';
import { IInputPassword } from '../type';

describe('__component: InputPassword', () => {
  const props: IInputPassword = {
    id: 'inputPasswordID',
    name: 'inputPassword',
  };
  const label = 'label';
  const mockSubmit = jest.fn();
  const user = userEvent.setup();

  const getComponentWithFormik = withFormik(InputPassword, props);

  afterEach(cleanup);

  it('should render correctly', () => {
    render(getComponentWithFormik(mockSubmit));

    const componentAInput = screen.getByTestId('InputPassword');
    const invisibleSVGIcon = screen.getByTitle('invisible');
    const visibleSVGIcon = screen.queryByTitle('visible');

    expect(invisibleSVGIcon).toBeInTheDocument();
    expect(visibleSVGIcon).toBeNull();
    expect(componentAInput).toBeInTheDocument();
  });

  it('should handle "label" property correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label }));

    const element = screen.getByText(label);

    expect(element.tagName).toBe('LABEL');
  });

  it('should handle "placeholder" property correctly', () => {
    const placeholder = 'placeholder';
    render(getComponentWithFormik(mockSubmit, { placeholder }));

    const element = screen.queryByPlaceholderText(placeholder);

    expect(element).toBeInTheDocument();
  });

  it('should handle "id" property correctly', () => {
    render(getComponentWithFormik(mockSubmit, { label }));

    const inputEl = screen.getByLabelText(label);
    const labelEl = screen.getByText(label);

    expect(inputEl).toHaveAttribute('id', props.id);
    expect(labelEl).toHaveAttribute('for', props.id);
  });

  it('should switch between input types "text/password" correctly after pressing the input visibility button', async () => {
    render(getComponentWithFormik(mockSubmit, { label }));

    const visibilityInputButton = screen.getByRole('button');

    await user.click(visibilityInputButton);

    const invisibleSVGIcon = screen.getByTitle('visible');
    const visibleSVGIcon = screen.queryByTitle('invisible');

    expect(invisibleSVGIcon).toBeInTheDocument();
    expect(visibleSVGIcon).toBeNull();
  });
});
