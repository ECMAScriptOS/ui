export interface IInputPassword {
  /**
   * Input id attribute
   */
  id: string;
  /**
   * Content of input placeholder
   */
  placeholder?: string;
  /**
   * Label content
   */
  label?: string;
  /**
   * Name of input
   */
  name: string;
  /**
   * Password strength validation
   */
  validationMessage?: string;
  /**
   * If true input will be disabled
   */
  disabled?: boolean;
}
