import { FC, HTMLAttributes, useEffect, useState } from 'react';
import { Field, FieldProps, useField } from 'formik';
import { ReactComponent as VisibleIcon } from 'assets/icons/common/visible.svg';
import { ReactComponent as InvisibleIcon } from 'assets/icons/common/invisible.svg';

import { ValidationAlertType } from '../ValidationAlert/type';
import { IInputPassword } from './type';
import {
  StyledInput,
  StyledInputWrapper,
  StyledLabel,
  StyledToggleVisibilityButton,
  StyledValidationAlert,
  StyledWrapper,
} from './InputPassword.styles';

export const InputPassword: FC<
  IInputPassword & HTMLAttributes<HTMLInputElement>
> = ({
  id,
  name,
  label,
  placeholder,
  validationMessage,
  disabled,
  className,
  ...rest
}) => {
  const [passwordIsVisible, setPasswordIsVisible] = useState(false);
  const [validation, setValidation] = useState<{
    type: ValidationAlertType;
    message: string;
  } | null>(null);

  const { error } = useField(name)[1];

  const togglePasswordVisibility = () => {
    setPasswordIsVisible((prevState) => !prevState);
  };

  const parseValidationMessage = (message: string) => {
    const validationContent = message.split(':')[1];

    if (message.match(/^Error:/i)) {
      setValidation({ type: 'error', message: validationContent });
    } else if (message.match(/^Warning:/i)) {
      setValidation({ type: 'warning', message: validationContent });
    } else {
      setValidation({ type: 'success', message: validationContent });
    }
  };

  useEffect(() => {
    const message = error || validationMessage;

    if (message) {
      parseValidationMessage(message);
    } else {
      setValidation(null);
    }
  }, [error, validationMessage]);

  return (
    <Field name={name}>
      {({ field, meta }: FieldProps) => (
        <StyledWrapper data-testid="InputPassword" className={className}>
          {label && <StyledLabel htmlFor={id}>{label}</StyledLabel>}
          <StyledInputWrapper
            validationType={meta.touched ? validation?.type : undefined}
          >
            <StyledInput
              id={id}
              type={passwordIsVisible ? 'text' : 'password'}
              placeholder={placeholder}
              disabled={disabled}
              {...field}
              {...rest}
            />
            <StyledToggleVisibilityButton
              icon={
                passwordIsVisible ? (
                  <VisibleIcon title="visible" />
                ) : (
                  <InvisibleIcon title="invisible" />
                )
              }
              rounded
              handleClick={togglePasswordVisibility}
              passwordIsVisible={passwordIsVisible}
              disabled={disabled}
            />
          </StyledInputWrapper>
          {meta.touched && validation && (
            <StyledValidationAlert
              type={validation.type}
              message={validation.message}
            />
          )}
        </StyledWrapper>
      )}
    </Field>
  );
};
