import { FunctionComponent, ReactElement } from 'react';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Formik } from 'formik';
import { BrowserRouter } from 'react-router-dom';

import { formikSubmitEvent } from '../../types';

// ToDo: Need to test with validation errors
export const withFormik =
  <T extends { name: string }>(
    Component: FunctionComponent<T>,
    defaultProps: T
  ) =>
  // eslint-disable-next-line react/display-name
  (onSubmit: formikSubmitEvent, props?: Partial<T>) =>
    (
      <Formik initialValues={{ [defaultProps.name]: '' }} onSubmit={onSubmit}>
        <Component {...defaultProps} {...props} />
      </Formik>
    );

export const renderWithRouter = (Component: ReactElement, route = '/') => {
  window.history.pushState({}, 'Test page', route);

  return {
    user: userEvent.setup(),
    ...render(Component, { wrapper: BrowserRouter }),
  };
};
