import { getDaysOfMonth } from './getDaysOfMonth';

export const getInputDateString = (
  day: number,
  month: number,
  year: number
) => {
  const dateHaseInvalidCharacters =
    !day.toString().match(/^\d+$/) || !year.toString().match(/^\d+$/);
  const january = 0;
  const december = 11;

  if (dateHaseInvalidCharacters || month < january || month > december) {
    return '';
  }

  const daysOfMonth = getDaysOfMonth(month, year);

  if (day > daysOfMonth || day < 1) {
    return '';
  }

  const date = new Date(year, month, day);

  return `${date.getFullYear()}-${(date.getMonth() + 1)
    .toString()
    .padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`;
};
