import { checkPasswordStrength } from '../checkPassowrdStrength';

describe('__function: checkPasswordStrength', () => {
  const mockValidation = {
    passwordIsShortWarning: 'Password is short',
    passwordConsistUserInfoWarning: 'Password consists of user info',
    passwordIsStrong: 'Password is strong',
  };
  const firstName = 'testName';

  it('should return warning message if password consists of less then 12 characters', () => {
    const message = checkPasswordStrength(
      { password: 'test123456' },
      mockValidation
    );

    expect(message).toBe(`Warning:${mockValidation.passwordIsShortWarning}`);
  });

  it('should return warning message if password consists of user info', () => {
    const message = checkPasswordStrength(
      { password: '123testName123456', firstName },
      mockValidation
    );

    expect(message).toBe(
      `Warning:${mockValidation.passwordConsistUserInfoWarning}`
    );
  });

  it('should return success message if password is strong', () => {
    const message = checkPasswordStrength(
      { password: 'testPassword123456', firstName },
      mockValidation
    );

    expect(message).toBe(`Success:${mockValidation.passwordIsStrong}`);
  });
});
