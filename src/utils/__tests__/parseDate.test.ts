import { parseDate } from '../parseDate';

describe('__function: parseDate', () => {
  const NOW = new Date(Date.now());
  const { minutes, hours, day, month, year, dayOfWeek } = parseDate(
    String(NOW)
  );

  it('should return minutes value correctly', () => {
    expect(NOW.getMinutes()).toBe(minutes);
  });

  it('should return hours value correctly', () => {
    expect(NOW.getHours()).toBe(hours);
  });

  it('should return day value correctly', () => {
    expect(NOW.getDate()).toBe(day);
  });

  it('should return month value correctly', () => {
    expect(NOW.getMonth()).toBe(month);
  });

  it('should return year value correctly', () => {
    expect(NOW.getFullYear()).toBe(year);
  });

  it('should return dayOfWeek value correctly', () => {
    expect(NOW.getDay()).toBe(dayOfWeek);
  });
});
