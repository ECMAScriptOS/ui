import { getInputDateString } from '../getInputDateString';

describe('__function: getInputDateString', () => {
  const mockData = {
    day: 1,
    year: 2000,
    month: 1,
  };
  const expectedString = `${mockData.year}-0${mockData.month + 1}-0${
    mockData.day
  }`;

  it('should return string correctly', () => {
    expect(
      getInputDateString(mockData.day, mockData.month, mockData.year)
    ).toBe(expectedString);
  });

  it('should return an empty string if day is specified incorrectly', () => {
    expect(getInputDateString(0, mockData.month, mockData.year)).toBe('');
    expect(getInputDateString(32, mockData.month, mockData.year)).toBe('');
  });

  it('should return an empty string if date hase incorrect characters', () => {
    expect(getInputDateString(2.2, mockData.month, mockData.year)).toBe('');
    expect(getInputDateString(mockData.day, -10, mockData.year)).toBe('');
    expect(getInputDateString(mockData.day, mockData.month, -2021)).toBe('');
  });
});
