import { getDaysOfMonth } from '../getDaysOfMonth';

describe('__function: getDaysOfMonth', () => {
  it('should return correctly parsed string', () => {
    expect(getDaysOfMonth(12, 2024)).toBe(31);
    expect(getDaysOfMonth(11, 2024)).toBe(30);
    expect(getDaysOfMonth(2, 2024)).toBe(29);
  });
});
