import { hexToRgba } from '../hexToRgba';

describe('__function: hexToRgba', () => {
  const hexColors = {
    color1: '#7f11e0',
    color2: '#1a7fe2',
  };

  const expectedRgbaColors = {
    color1: 'rgba(127, 17, 224, 1)',
    color2: 'rgba(26, 127, 226, 0.7)',
  };

  it('should return correctly rgba color', () => {
    expect(hexToRgba(hexColors.color1)).toBe(expectedRgbaColors.color1);
    expect(hexToRgba(hexColors.color2, 0.7)).toBe(expectedRgbaColors.color2);
  });

  it("should return null if hex color isn't correct", () => {
    expect(hexToRgba('#zf11e0')).toBe(null);
    expect(hexToRgba('#zf1')).toBe(null);
  });
});
