export const parseDate = (value: string | number) => {
  const date = new Date(value);

  return {
    minutes: date.getMinutes(),
    hours: date.getHours(),
    day: date.getDate(),
    dayOfWeek: date.getDay(),
    month: date.getMonth(),
    year: date.getFullYear(),
  };
};
