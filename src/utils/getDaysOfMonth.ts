export const getDaysOfMonth = (month: number, year: number) =>
  new Date(year, month, 0).getDate();
