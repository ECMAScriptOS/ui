export const checkPasswordStrength = (
  {
    password,
    firstName,
    lastName,
  }: {
    password: string;
    firstName?: string;
    lastName?: string;
  },
  translations: {
    passwordIsShortWarning: string;
    passwordConsistUserInfoWarning: string;
    passwordIsStrong: string;
  }
) => {
  const strongPasswordMinLength = 12;

  if (password.length < strongPasswordMinLength) {
    return `Warning:${translations.passwordIsShortWarning}`;
  } else if (
    (firstName && password.toLowerCase().includes(firstName.toLowerCase())) ||
    (lastName && password.toLowerCase().includes(lastName.toLowerCase()))
  ) {
    return `Warning:${translations.passwordConsistUserInfoWarning}`;
  } else {
    return `Success:${translations.passwordIsStrong}`;
  }
};
