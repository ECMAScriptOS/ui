export const hexToRgba = (hex: string, alpha = 1) => {
  const regExp = /(^#[0-9A-F]{6}$)/i;

  const isHexColor = regExp.test(hex);

  if (!isHexColor) return null;

  const [r, g, b] = hex.match(/\w\w/g)!.map((x) => parseInt(x, 16));

  return `rgba(${r}, ${g}, ${b}, ${alpha})`;
};
