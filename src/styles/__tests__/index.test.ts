import { getFlexDirection } from '../common';

describe('__function: getFlexDirection', () => {
  it('should return correct value without arguments', () => {
    const result = getFlexDirection();

    expect(result).toEqual('row-reverse');
  });

  it('should return correct value with "labelPosition=right"', () => {
    const result = getFlexDirection('right');

    expect(result).toEqual('row-reverse');
  });

  it('should return correct value with "labelPosition=top"', () => {
    const result = getFlexDirection('top');

    expect(result).toEqual('column');
  });

  it('should return correct value with "labelPosition=bottom"', () => {
    const result = getFlexDirection('bottom');

    expect(result).toEqual('column-reverse');
  });

  it('should return correct value with "labelPosition=left"', () => {
    const result = getFlexDirection('left');

    expect(result).toEqual('row');
  });
});
