import { css } from 'styled-components';

import { LabelPositionType } from '../types';

export const FlexColumnDirection = css`
  display: flex;
  flex-direction: column;
`;

export const FlexCenteredXY = css`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const getFlexDirection = (labelPosition?: LabelPositionType) => {
  if (labelPosition === 'top') return 'column';
  if (labelPosition === 'bottom') return 'column-reverse';
  if (labelPosition === 'left') return 'row';

  return 'row-reverse';
};
