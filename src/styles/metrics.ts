export const INPUT_PADDINGS = {
  vertical: '0.5rem',
  left: '0.7rem',
  right: '1.4rem',
};

export const PADDINGS = {
  vertical: '0.5rem',
  vertical_sm: '0.2rem',
  horizontal: '0.7rem',
  horizontal_sm: '0.4rem',
};

export const BORDER_RADIUS = {
  normal: '14px',
};
