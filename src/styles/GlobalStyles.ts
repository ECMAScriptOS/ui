import { createGlobalStyle } from 'styled-components';

import { COLORS, FONT_COLORS } from './colors';

export const GlobalStyles = createGlobalStyle`
  *,
  *:before,
  *:after {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  
  html {
    font-size: 16px;
  }

  body {
    font-family: 'Roboto', 'Arial', sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    color: ${FONT_COLORS.primary};
  }
  
  input, textarea {
    outline: none;
    border: none;
    font-size: 1rem;
    background-color: ${COLORS.white};
    color: ${FONT_COLORS.primary};
    
    &::placeholder {
      color: ${FONT_COLORS.gray};
    }
    
    &:-webkit-autofill, &:-webkit-autofill:focus {
      -webkit-box-shadow: 0 0 0px 1000px ${COLORS.white} inset;
      -webkit-text-fill-color: ${FONT_COLORS.primary};
    }
  }
  
  fieldset {
    border: none;
  }
  
  button {
    border: none;
    background-color: transparent;
    cursor: pointer;
    color: ${FONT_COLORS.primary};
  }

  ul {
    list-style: none;
  }

  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
    monospace;
  }
  
  ::-webkit-scrollbar {
    width: 4px;
    height: 4px;
  }

  ::-webkit-scrollbar-thumb {
    background-color: ${COLORS.primary};
  }
  
  ::-webkit-scrollbar-track {
    background-color: ${COLORS.gray};
  }
`;
