export const COLORS = {
  primary: '#21170B',
  secondary: '#BD7D2F',
  error: '#F02A2A',
  gray: '#A09486',
  white: '#F3F3EF',
  warning: '#E3D50E',
  success: '#65A341',
};

export const FONT_COLORS = {
  primary: '#21170B',
  secondary: '#BD7D2F',
  error: '#F02A2A',
  gray: '#A09486',
  white: '#F3F3EF',
  warning: '#E3D50E',
  success: '#65A341',
};
