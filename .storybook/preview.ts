import type { Preview } from '@storybook/react';
import { GlobalStyles } from '../src/styles/GlobalStyles';
import { setConsoleOptions } from '@storybook/addon-console';
import { withThemeFromJSXProvider } from '@storybook/addon-themes';

const preview: Preview = {
  globalTypes: {},
};

export const parameters = {
  layout: 'fullscreen',
};

export const decorators = [
  withThemeFromJSXProvider({
    GlobalStyles,
  }),
  setConsoleOptions,
];

export default preview;
